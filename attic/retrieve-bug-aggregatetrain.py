
# coding: utf-8

# - read tifs
# - aggregate into tiles
# - write csv or pickle

# ## installation
# ### dependencies
# try apt-get install python-rasterio python-geopandas. On Ubuntu 16.04 this brings in rasterio-0.31
# 
# - install/upgrade via pip:
#     - pip install --upgrade enum34 requests rasterio geopandas
#  
# If pip install rasterio fails:
# rasterio/_base.c:287:22: fatal error: cpl_conv.h: No such file or directory
# just apt-get install libgdal-dev
# 
# 

# In[12]:


# x read geotif. We have many.
# x read shp. Only one, but with many classes in them.
# x rasterise shp, use as filter for tif
# remove 0 label
# - decompose into cells, aggregate: compute histogram
# write aggregated CSV or pickle

# 1 working code
# 2 bugfree code
# 3 documented code
# 4 optimised code

# we want: features vs samples
# - train_df: only shp regions 
# - pred_df: everywhere

# list_of_tiles = create list of tiles. Should be standard operation for raster data.

# for the_tif in tifs:
#    read the_tif
#    for the_tile in list_of_tiles:
#        aggregate data on the_tile
# 


# In[17]:


get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

import geopandas
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats.mstats
from pdb import pm
import numpy.testing as npt
from sklearn import svm
from sklearn import tree
from sklearn import ensemble
from sklearn.model_selection import cross_val_score
from sklearn.cluster import KMeans
import string
from tictoc import Tic
from common import FeatureCallback, FeatureHistogram, Aggregator,                    path_to_landforms_shp, figsize,                    get_equal_freq_bins, interquatrile_range, BlockInfo,                    MapPlotter, map_class_ID_to_colour,                    load_src_and_band
import sys

# Plot style for Comp & Geosci
import pub
pub.SetPlotRC_CompGeosci()

import logging

logging.basicConfig(level=logging.WARN,
                    format='%(levelname)s:%(message)s')

# read land classes from shapefile
shapefile = geopandas.read_file(path_to_landforms_shp)
geoms = shapefile.geometry.values # list of shapely geometries
#geoms = geoms[0:3] # use just two classes
#geoms = [geoms[4], geoms[6], geoms[8]]
#geoms = [geoms[4], geoms[5], geoms[6],  geoms[7], geoms[8]]
#geoms = [geoms[4], geoms[8]]



# In[18]:


the_geom = geoms[5]
import shapely.affinity
the_geom = shapely.affinity.translate(the_geom, xoff=2.0, yoff=0.0, zoff=0.0)
geoms = [the_geom, geoms[6]]
#geoms = [the_geom]


# In[20]:


relief_dict = {6: (46, 56, 66, 76), 
               5: (35, 45, 55, 65, 75, 34, 44, 54, 64, 74),
               3: (33, 43, 53, 63),
               2: (22, 32, 42, 52),
               1: (11, 21, 31, 41),
               4: (51, 61, 71, 62, 72, 73)}

def relief_histogram(data):
    """map relief. Accept flattened data, return a vector"""

    # map data to relief_class
    keys = [key if item in value else 0 for item in data for key, value in relief_dict.iteritems()]
    
    # compute histogram of relief class
    bins = [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5]
    return np.histogram(keys, bins=bins)[0]

def test_relief_histogram():
    npt.assert_array_equal(relief_histogram([11]), [1, 0, 0, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([22]), [0, 1, 0, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([33]), [0, 0, 1, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([51]), [0, 0, 0, 1, 0, 0])
    npt.assert_array_equal(relief_histogram([35]), [0, 0, 0, 0, 1, 0])
    npt.assert_array_equal(relief_histogram([46]), [0, 0, 0, 0, 0, 1])

test_relief_histogram()


# ## Assemble featureX dict

# In[21]:


###############################################################################
## Assemble a dictionary of FeatureX classes, which describe potential features
## for our model, e.g. mrVBF divided into 10 bins.
## We assemble all possible features here, and later pick those which we 
## actually use for training.
###############################################################################

fdict = {}

def add_to_dict(ftif):
    ftif.add_to_dict(fdict)

if 0:
    the_tif = "elev.tif"
    add_to_dict(FeatureCallback(the_tif, ["eskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["ekurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["eiqr"], callback=interquatrile_range))

bins = np.arange(11)
add_to_dict(FeatureHistogram("mrvbf.tif", bins=bins, feature_prefix='vbf'))
add_to_dict(FeatureHistogram("mrrtf.tif", bins=bins, feature_prefix='rtf'))


# equal freq binning
if 1:
    the_tif = "profcurv.tif"
    n_bins = 5
    src, data, valid_data = load_src_and_band(the_tif)
    bins = get_equal_freq_bins(valid_data, n_bins)
    add_to_dict(FeatureHistogram(the_tif, bins=bins, feature_prefix='profcurv', feature_postfixes=np.arange(n_bins)+1))

##the_tif = "plancurv.tif"
##n_bins = 5
##src, data, valid_data = load_src_and_band(the_tif)
##bins = get_equal_freq_bins(valid_data, n_bins)
##add_to_dict(FeatureHistogram(the_tif, bins=bins, feature_prefix='plancurv', feature_postfix=np.arange(n_bins)+1)

the_tif = "twi.tif" # topo wetness index
add_to_dict(FeatureCallback(the_tif, ["tmean"], callback=np.mean))

if 1:
    the_tif = "slope.tif"
    add_to_dict(FeatureCallback(the_tif, ["sskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["skurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["siqr"], callback=interquatrile_range))

if 1:
    the_tif = "wei.tif" # wind exposure index
    add_to_dict(FeatureCallback(the_tif, ["wskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["wkurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["wiqr"], callback=interquatrile_range))
    add_to_dict(FeatureCallback(the_tif, ["wmean"], callback=np.mean))

# slopeRCL
if 1:
    # "slopeLE","slopeVG","slopeGE","slopeMO","slopeST","slopeVS",
    slope_bins  = [16, 26, 36, 46, 56, 66, 76]
    slope_names = ["LE", "VG", "GE", "MO", "ST", "VS", "CL"]
    bins=slope_bins
    #{"LE": 16, "VG": 26, "GE": 36, "MO": 46, "ST": 56, "VS": 66, "CL": 76}
    add_to_dict(FeatureHistogram("sloperelcl.tif", bins=slope_bins, feature_prefix='slope', feature_postfixes=slope_names[:-1]))

#"reliefP","reliefR","reliefL","reliefH","reliefM",
# relief
if 0:
    # this really takes a long time to aggregate. Would need optimisation.
    add_to_dict(FeatureCallback("sloperelcl.tif", feature_names=['relP', 'relR', 'relL', 'relH', 'relM', 'relB'], callback=relief_histogram))

the_tif = "slope.tif"
add_to_dict(FeatureCallback(the_tif, ["sskewness"], callback=scipy.stats.mstats.skew))


## Pick which features to train on. Either use all features...
ftifs = [val for val in fdict.itervalues()] 

## ... or cherry-pick
ftifs = [ fdict['vbf'] ]



# ## Aggregate training data

# In[22]:


###############################################################################
## Aggregate training data into blocks
###############################################################################

print "aggregate training ...", 

step = 50; size = 561 
#step = 25; size = 91 # quick
#step = 12; size = 121
#step = 6; size = 41
#step = 3; size = 3

block_info = BlockInfo(size_x=size, size_y=size, step_x=step, step_y=step)
#np.set_printoptions(precision=3, suppress=True)
aggregator = Aggregator(block_info, ftifs, geoms, min_valid_pixel_ratio=0.1)

training_data = aggregator.aggregate(csv_filename=None, ax=None)
print "done."

size_metres = np.array(aggregator.block_size_metres())

print "size, step", size, step
print "average block size %5.2f km" % (size_metres.mean() / 1000.)

map_step_to_scatter_size = {400:125, 200:30.6, 100: 7.8, 50: 1.95, 25: 0.4861} # linewidths=0
scatter_size = map_step_to_scatter_size[step]


# # Train

# In[23]:



#clf = ensemble.RandomForestClassifier()
X = training_data[:,:-3]
y = training_data[:,-3]


# ### SVM hyperparam optimisation



###############################################################################
## Train. Follow sklearn terminology: 
## X is matrix of training data, y is vector of labels
###############################################################################
train_array = training_data[:,:-3]
lonlat = training_data[:,-2:]

#clf = tree.DecisionTreeClassifier()
#clf_name = "DT"

plot_distance = False

#clf = svm.SVC(kernel="poly", degree=4) # BS
#clf = svm.SVC() # 80%
#clf = svm.NuSVC() # need param
#clf = svm.SVC(kernel="rbf") # 83%
#clf = svm.SVC(kernel="rbf") # 83%
clf = ensemble.RandomForestClassifier(n_estimators = 100, random_state = 42)
clf_name = "RF100"
#clf = svm.SVC(C=2.33, gamma=1.67)
#clf_name = "SVC"

print "training...", clf_name
X = training_data[:,:-3]
y = training_data[:,-3]
lonlat = training_data[:,-2:]

## k-fold cross-validation.
## k can't be greater than smallest sample size, so 
## count samples for each class
hist, _ = np.histogram(y, bins=np.arange(0,len(geoms)))
k = min(hist.min(), 10)
assert(hist.sum() == len(y))

timer = Tic(verbose=True)
if k >= 2:
    print "%i-fold CV" % k
    scores = cross_val_score(clf, X, y, cv=k)
    np.set_printoptions(precision=0)
    param_str = "size: %i  step: %i %s Acc: %0.3f (+/- %0.2f)\n%s"          % (size, step, clf_name, scores.mean(), scores.std() * 2,
           string.join(["%3i" % int(item) for item in scores*1000]))
else:
    param_str = "size: %i  step: %i %s no cross-valid possible"          % (size, step, clf_name)
print param_str
print "CV took", timer.toc()

timer = Tic(verbose=True)
clf.fit(X, y)
print "training took", timer.toc()

print "done. "


# ## plot training

# In[28]:



#scatter_size = 5 # ... override anyway
c_y = map_class_ID_to_colour(y)

fig, ax = plt.subplots(1,1,figsize=figsize)
mp = MapPlotter(lonlat, scatter_size, geoms, map_class_ID_to_colour, ax)
mp.load_background_tif("mrvbf.tif")
mp.plot_block(size)
ax.text(0., 1.05, param_str, transform=ax.transAxes)

map_filename = "AFO_pred%s_size%03i_step%03i.png" % (clf_name, size, step)
#mp.plot_background()
mp.plot_geoms()
mp.plot(c_y, alpha=1)
ax.legend(ncol=9, loc="lower left", 
     markerscale=0.02, columnspacing=0.5,
     handlelength=0.2, handletextpad=0.3, frameon=False, 
     bbox_to_anchor=(0.23, -0.029))



# In[28]:
sys.exit(0)

clf.get_params()


# In[38]:


the_geom = geoms[5]
import shapely.affinity
print shapely.affinity.translate(the_geom, xoff=2.0, yoff=0.0, zoff=0.0)


# ## Aggregate entire map

# In[23]:


############################################################################### 
## Aggregate entire map, by passing geoms=None
###############################################################################
t_per_op = 2.7e-7 # estimate
size_px = 10000 # FIXME: should get this from src
n_op = (size_px/float(step))**2 * size**2
t_predicted = t_per_op * n_op
timer = Tic(verbose=True)
print "aggregate entire map, predicted runtime %i min..." % (t_predicted / 60.)
aggregator_map = Aggregator(block_info, ftifs, geoms=None)
entire_data = aggregator_map.aggregate(ax=None, csv_filename=None)
print "Done. Took ", timer.toc()


# ## predict entire map and plot

# In[117]:


###############################################################################
## Predict entire map, then plot
###############################################################################
from common import MapPlotter
X = entire_data[:,:-3]
lonlat_map = entire_data[:,-2:]
predicted = clf.predict(X)
c_predicted = map_class_ID_to_colour(predicted)

## plot final map. We use matplotlib's scatter plot. 
## scatter_size is fine-tuned such that we still get a somewhat seamless map.
#map_step_to_scatter_size = {50: 880, 25: 220, 12:43, 6:8.4, 3:2}

#map_step_to_scatter_size = {100: 2.4, 50: 0.6, 25: 0.15} # production ready figures
#map_step_to_scatter_size = {200:31.1, 100: 7.8, 50: 1.95, 25: 0.4861} # linewidths=0
map_step_to_scatter_size = {400:125, 200:30.6, 100: 7.8, 50: 1.95, 25: 0.4861} # linewidths=0
scatter_size = map_step_to_scatter_size[step]


# In[118]:


#scatter_size = 30.60 #0.4861 # ... override anyway

# TEMP:
quick=0
if quick:
    mask = lonlat_map[:,1] > -27.5
    lonlat_map = lonlat_map[mask]
    predicted = predicted[mask]

#predicted = np.random.randint(low=0, high=8, size=len(predicted))
c_predicted = map_class_ID_to_colour(predicted)

from common import MapPlotter, figsize
import rasterio
from affine import Affine

if plot_distance:
    # Returns distance to the decision boundary, for each class.
    # The decision boundary separates a class from the rest.
    distance = clf.decision_function(X)

    # If we have only two classes, decision_function returns a single vector,
    # in which negative values correspond to one side of the decision boundary, 
    # and positive to the other. Re-cast into a matrix for compatibility.
    if len(geoms) == 2:
        distance = np.vstack((-distance, distance)).transpose()


fig, ax = plt.subplots(1,1,figsize=figsize)
mp = MapPlotter(lonlat_map, scatter_size, geoms, map_class_ID_to_colour, ax)
mp.load_background_tif("mrvbf.tif")
mp.plot_block(size)
ax.text(0., 1.05, param_str, transform=ax.transAxes)

map_filename = "AFO_pred%s_size%03i_step%03i.png" % (clf_name, size, step)
#mp.plot_background()
mp.plot_geoms()
mp.plot(c_predicted, alpha=1)

ax.legend(ncol=9, loc="lower left", 
     markerscale=0.02, columnspacing=0.5,
     handlelength=0.2, handletextpad=0.3, frameon=False, 
     bbox_to_anchor=(0.23, -0.029))

plt.savefig(map_filename, dpi=200)
#mask_list = mp.plot_distance(predicted, distance, threshold=1.5, filename=map_filename, alpha=0.5)

#for i in range(len(geoms)):
#    print "%02i  %5i  %5i" % (i, (y == i).sum(), (predicted == i).sum())

print "All done."


# In[30]:


tot = 0
thresh = 0.5
for i, mask in enumerate(mask_list):
    this_class_distance = distance[:,i]
    print mask.shape, 
    print mask.sum(),
    print (this_class_distance > thresh).sum()
    tot += mask.sum()
print tot


# In[ ]:


plt.hist(predicted, bins=np.arange(10)-0.5)


# In[ ]:


distance.shape
fig, axes = plt.subplots(1,3, figsize=(15,10))
axes = axes.flatten()
for i, ax in enumerate(axes):
    ax.hist(distance[:,i], bins=100)
    


# In[ ]:


class MapPlotter2(object):
    def __init__(self, lonlat_map, scatter_s, geoms, colour_mapper_callback, ax):
        self.lonlat_map = lonlat_map
        self.scatter_s = scatter_s
        self.colour_mapper_callback = colour_mapper_callback
        self.geoms = geoms
        self.band = None
        self.ax = ax
        self.src = None
        logging.warn("FIXME: mask where BG image is blank")
        
    def load_background_tif(self, the_tif):
        self.src, band, _ = load_src_and_band(the_tif, band_ID=1)
        band[band==self.src.nodata] = 12
        self.band = band
        self.extent = rasterio.plot.plotting_extent(self.src)
        
    def plot_block(self, size, transform=None):
        if transform is None:
            if self.src is None:
                raise ValueError("Can't plot block. Either provide transform, "                                 "or call load_background_tif() beforehand")
            transform = self.src.affine
        T1 = transform * Affine.translation(0.5, 0.5)
        RC = np.array([[0,0], [0,size], [size,size], [size,0], [0,0]])
        RC += 10
#            C, R = 
        X, Y = ((RC[:,1], RC[:,0]) * T1)
        self.ax.plot(X, Y, 'y-', lw=3)

    def plot_background(self):
        if self.band is not None:
            self.ax.imshow(self.band, cmap=plt.cm.gray, extent=self.extent) # plot

    def plot_geoms(self):
        
        for i, the_class in enumerate(self.geoms):
            #plot_class(the_class, ax1, color='k')
            x, y = the_class.exterior.xy
            self.ax.plot(x, y, color=self.colour_mapper_callback(i), lw=4,
                         label=i)
            self.ax.plot(x, y, 'k--', lw=4)

    def plot(self, c_predicted, filename=None, alpha=1):
        """plot classification"""
        #fig, (ax1) = plt.subplots(1,1, figsize=figsize)
        #ax1.scatter(lonlat[:,0], lonlat[:,1], marker='o', s=10, c=c_label, edgecolors='face')
        self.ax.scatter(self.lonlat_map[:,0], self.lonlat_map[:,1], s=self.scatter_s, marker='s', c=c_predicted, edgecolors='face', alpha=alpha)
        self.ax.set_xlim(self.extent[0:2])
        self.ax.set_ylim(self.extent[2:])
        self.ax.set_aspect('equal')
        #self.ax.get_xaxis().set_visible(False)
        #self.ax.get_yaxis().set_visible(False)
        self.ax.legend()
        
        if filename is not None:
            plt.savefig(filename)

    def plot_distance(self, predicted, distance_metric, threshold, filename=None, alpha=1):
        """plot classification, but only if their distance metric > threshold"""
        if distance_metric.shape[1] < len(self.geoms):
            raise ValueError("distance_metric contains %i classes, but we have %i classes in geoms. "                               "Most likely, one or more classes have not been found by the ML algorithm."                              % (distance_metric.shape[1], len(self.geoms)))
        for i in range(len(self.geoms)):
            this_class_distance = distance_metric[:,i]
            mask = np.logical_and(this_class_distance > threshold,
                                  predicted == i)# the higher this threshold, the more certain the algo is

            self.ax.scatter(self.lonlat_map[mask,0], self.lonlat_map[mask,1], 
                       c=map_class_ID_to_colour(i),
                       s=self.scatter_s, marker='s', edgecolors='face', alpha=0.5)
            #the_dX = this_class_distance
            #print "k", the_dX.min(), the_dX.max()

        self.ax.set_xlim(self.extent[0:2])
        self.ax.set_ylim(self.extent[2:])
        self.ax.set_aspect('equal')
        self.ax.legend()

        if filename is not None:
            plt.savefig(filename)



# In[ ]:


aggregator.hdr

