#!/usr/bin/env python2.7
# coding: utf-8

# - read tifs
# - aggregate into tiles
# - write csv or pickle

# ## installation
# ### dependencies
# try apt-get install python-rasterio python-geopandas. On Ubuntu 16.04 this brings in rasterio-0.31
# 
# - install/upgrade via pip:
#     - pip install --upgrade enum34 requests rasterio geopandas
#  
# If pip install rasterio fails:
# rasterio/_base.c:287:22: fatal error: cpl_conv.h: No such file or directory
# just apt-get install libgdal-dev
# 
# 

# In[ ]:


# x read geotif. We have many.
# x read shp. Only one, but with many classes in them.
# x rasterise shp, use as filter for tif
# remove 0 label
# - decompose into cells, aggregate: compute histogram
# write aggregated CSV or pickle

# 1 working code
# 2 bugfree code
# 3 documented code
# 4 optimised code

# we want: features vs samples
# - train_df: only shp regions 
# - pred_df: everywhere

# list_of_tiles = create list of tiles. Should be standard operation for raster data.

# for the_tif in tifs:
#    read the_tif
#    for the_tile in list_of_tiles:
#        aggregate data on the_tile
# 


# In[ ]:


import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats.mstats
from pdb import pm
import logging
from sklearn import svm
from sklearn import tree
from sklearn.model_selection import cross_val_score
from sklearn.cluster import KMeans

from common import FeatureCallback, FeatureHistogram, Aggregator, \
                   load_src_and_band, path_to_landforms_shp, \
                   get_equal_freq_bins, interquatrile_range, BlockInfo, \
                   Entire_map_plotter, map_class_ID_to_colour
import common as ag
#import argparse
#parser = argparse.ArgumentParser(description="")
#parser.add_argument("-", action="store", type=float, default=0, help="")
#parser.add_argument("-", '--', action="store_true", help="show plot")
#parser.add_argument("-v", "--verbose", dest="verbose_count", action="count",
#                    default=0, help="increase verbosity")
#parser.add_argument("-q", "--quiet", dest="quiet_count", action="count",
#                    default=0, help="decrease verbosity")
#parser.add_argument('infiles', metavar='file', type=str, nargs='*',
#               help='the file')
#args = parser.parse_args()
class Args(object):
    verbose_count = 0
    quiet_count = 0

args = Args()
level = (max(3 - args.verbose_count + args.quiet_count, 0) * 10)
logging.basicConfig(level=level, # defaults to 30, i.e. WARN
                    format='%(levelname)s:%(message)s')
np.set_printoptions(precision=4, suppress=True)#print data.shape


# In[ ]:


print level

# map class to color
_class_colours = ['#ff00ff', '#b0171f',
          '#ffdb00',
          '#ca02bd',
          '#3d59ab',
          '#13adf9',
          '#3f7543',
          '#00cd66',
          '#b99d00',
          '#ff00ff']


# read classes from shapefile
shapefile = gpd.read_file(path_to_landforms_shp)
# extract the geometry in GeoJSON format
geoms = shapefile.geometry.values # list of shapely geometries


# ## collect TIFs

# In[ ]:


# x "slopeLE","slopeVG","slopeGE","slopeMO","slopeST","slopeVS",
#   "reliefP","reliefR","reliefL","reliefH","reliefM",
# x "vbf0","vbf1","vbf2","vbf3","vbf4","vbf5","vbf6","vbf7","vbf8","vbf9",
# x "rtf0","rtf1","rtf2","rtf3","rtf4","rtf5","rtf6","rtf7","rtf8","rtf9",
# x "profcurv1","profcurv2","profcurv3","profcurv4","profcurv5",
# x  "eiqr","siqr","wiqr", IQR for elev, slope, WEI
#### Interquartile range: if Q1 is 25 percentile, Q2 the median, Q3 the 75 percentile, then IQR = Q3 - Q1
# x "wmean",
# x "ekurtosis","skurtosis","wkurtosis",
# x "eskewness","sskewness","wskewness",
#   "etexture","stexture",
# x "label","s1","s2"

fdict = {}
# skip elev for now. Elev has less NA than the other derived quantities, and is therefore incompatible.

def add_to_dict(ftif):
    ftif.add_to_dict(fdict)

if 1:
    the_tif = "elev.tif"
    add_to_dict(FeatureCallback(the_tif, ["eskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["ekurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["eiqr"], callback=interquatrile_range))

bins = np.arange(11)
add_to_dict(FeatureHistogram("mrvbf.tif", bins=bins, feature_prefix='vbf'))
add_to_dict(FeatureHistogram("mrrtf.tif", bins=bins, feature_prefix='rtf'))


# equal freq binning
the_tif = "profcurv.tif"
n_bins = 5
src, data, valid_data = load_src_and_band(the_tif)
bins = get_equal_freq_bins(valid_data, n_bins)
add_to_dict(FeatureHistogram(the_tif, bins=bins, feature_prefix='profcurv', feature_postfixes=np.arange(n_bins)+1))

##the_tif = "plancurv.tif"
##n_bins = 5
##src, data, valid_data = load_src_and_band(the_tif)
##bins = get_equal_freq_bins(valid_data, n_bins)
##add_to_dict(FeatureHistogram(the_tif, bins=bins, feature_prefix='plancurv', feature_postfix=np.arange(n_bins)+1)

the_tif = "twi.tif" # topo wetness index
add_to_dict(FeatureCallback(the_tif, ["tmean"], callback=np.mean))

the_tif = "slope.tif"
add_to_dict(FeatureCallback(the_tif, ["sskewness"], callback=scipy.stats.mstats.skew))
add_to_dict(FeatureCallback(the_tif, ["skurtosis"], callback=scipy.stats.mstats.kurtosis))
add_to_dict(FeatureCallback(the_tif, ["siqr"], callback=interquatrile_range))

if 1:
    the_tif = "wei.tif" # wind exposure index
    add_to_dict(FeatureCallback(the_tif, ["wskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["wkurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["wiqr"], callback=interquatrile_range))
    add_to_dict(FeatureCallback(the_tif, ["wmean"], callback=np.mean))

# slopeRCL
if 1:
    # "slopeLE","slopeVG","slopeGE","slopeMO","slopeST","slopeVS",
    slope_bins  = [16, 26, 36, 46, 56, 66, 76]
    slope_names = ["LE", "VG", "GE", "MO", "ST", "VS", "CL"]
    bins=slope_bins
    #{"LE": 16, "VG": 26, "GE": 36, "MO": 46, "ST": 56, "VS": 66, "CL": 76}
    add_to_dict(FeatureHistogram("sloperelcl.tif", bins=slope_bins, feature_prefix='slope', feature_postfixes=slope_names[:-1]))

#"reliefP","reliefR","reliefL","reliefH","reliefM",

the_tif = "slope.tif"
add_to_dict(FeatureCallback(the_tif, ["sskewness"], callback=scipy.stats.mstats.skew))

## pick features to train on

ftifs = [val for val in fdict.itervalues()] # use all features
#ftifs = [ fdict['vbf'] ]
#

# ## aggregate for training
first = True
def mk_map(size, step):
    print "NEXT------------------------------------"
    print "size, step", size, step

    #block_info = BlockInfo(size_x=5, size_y=5, step_x=5, step_y=5); scatter_s=4.1
    #block_info = BlockInfo(size_x=11, size_y=11, step_x=5, step_y=5); scatter_s=4.1
    #block_info = BlockInfo(size_x=15, size_y=15, step_x=15, step_y=15); scatter_s=4.1*3*5.3
    #block_info = BlockInfo(size_x=15, size_y=15, step_x=5, step_y=5); scatter_s=4.1-0.01
    #block_info = BlockInfo(size_x=19, size_y=19, step_x=5, step_y=5); scatter_s=4.1
    #block_info = BlockInfo(size_x=5, size_y=5, step_x=25, step_y=25); scatter_s=60 # quick
    block_info = BlockInfo(size_x=size, size_y=size, step_x=step, step_y=step)
    
    np.set_printoptions(precision=3, suppress=True)
    
    the_geoms = geoms
    aggregator = Aggregator(block_info, ftifs, the_geoms)
    
    aggregator.count_data_points(ftifs[0].filename)
    print "aggregate training ...", 
    try:
        aggregator.aggregate(ax=None)
    except:
        print "AGGREGRATE FAILED."
        return
    
    print "done."
    training_data = aggregator.write_csv(write=False)
    
    
    # ## aggregate entire map
    
    
    print "aggregate entire map..."
    aggregator_map = Aggregator(block_info, ftifs, geoms=None)
    aggregator_map.count_data_points(ftifs[0].filename)
    aggregator_map.aggregate(ax=None)
    entire_data = aggregator_map.write_csv(write=False)
    print "Done. shape", entire_data.shape
    
    
    # ## train
    train_array = training_data[:,:-3]
    #labels = training_data[:,-3]
    lonlat = training_data[:,-2:]
    
    CLFs = {"DT_": tree.DecisionTreeClassifier(), 
            "lSV": svm.LinearSVC(),
            "SV_": svm.SVC()
            }

    CLFs = {"DT_": tree.DecisionTreeClassifier()}
    
    unsupervised = False
    #for n_clusters in range(2, 10):
    #    clf = KMeans(n_clusters=n_clusters, random_state=0) #.fit(X)
    #    clf_name = "km%i" % n_clusters
    
    for clf_name, clf in CLFs.iteritems():
        #clf = svm.SVC(kernel='poly', degree=3) # no good
        #clf = svm.SVC()
        
        # On the other hand, LinearSVC implements “one-vs-the-rest” multi-class strategy, 
        # thus training n_class models. 
        #clf = svm.LinearSVC() # better than SVC
        
        #clf = 
        print "training...", clf_name
        X = training_data[:,:-3]
        y = training_data[:,-3]
        lonlat = training_data[:,-2:]
        print "shape", train_array.shape

        # k-fold cross-validation.
        # k can't be greater than smallest sample size, so 
        # count samples for each class
        hist, _ = np.histogram(y, bins=np.arange(1,10))
        k = min(hist.min(), 10)
        if k >= 2:
            print "%i-fold CV" % k
            if unsupervised:
                scores = cross_val_score(clf, X, cv=k)
            else:    
                scores = cross_val_score(clf, X, y, cv=k)
                    # param string
            param_s = "size: %i  step: %i %s Acc: %0.2f (+/- %0.2f) %s"  \
                % (size, step, clf_name, scores.mean(), scores.std() * 2,
                   str(scores))
        else:
            param_s = "size: %i  step: %i %s no cross-valid possible"  \
                % (size, step, clf_name)
            

        if unsupervised:
            clf.fit(X)
        else:
            clf.fit(X, y)
        print "done. "
        #clf.support_vectors_
        print "NaN?", np.isnan(train_array).any()
        
        
        # ## plot classification of training data
        if 0:
            predicted = clf.predict(X)
            fig, (ax0, ax1) = plt.subplots(1,2, figsize=(10,8), dpi=300)
            c_label = map_class_ID_to_colour(y)
            c_predicted = map_class_ID_to_colour(predicted)
            
            ax0.scatter(lonlat[:,0], lonlat[:,1], marker='.', c=c_label, edgecolors='face')
            ax1.scatter(lonlat[:,0], lonlat[:,1], marker='.', c=c_predicted, edgecolors='face')
        
        
        # ## Predict entire map
        
        X = entire_data[:,:-3]
        lonlat_map = entire_data[:,-2:]
        predicted = clf.predict(X)
        #c_label = [class_colours[int(item)] for item in labels]
        c_predicted = map_class_ID_to_colour(predicted)

        print param_s
        
        map_step_to_scatter_s = {50: 880, 25: 220, 12:43, 6:8.4}
        scatter_s = map_step_to_scatter_s[step]*1.1
        fig, ax = plt.subplots(1,1,figsize=ag.figsize)
        emp = Entire_map_plotter(lonlat_map, scatter_s, geoms, map_class_ID_to_colour, ax)
        emp.load_bg_tif("mrvbf.tif")
        emp.plot_block(src.affine, size)
        map_filename = "AFO_pred%s_size%03i_step%02i.png" % (clf_name, size, step)
        ax.text(0.3, 0.005, param_s, transform=ax.transAxes)
        emp.plot(c_predicted, filename=map_filename, alpha=0.5)
        global first
        if first:
            emp.plot(c_predicted, filename="mrvbf.png", alpha=0.)
            first = False

#        print "step %g scatter_s %g" % (step, scatter_s)
        
#steps = [50, 25, 12, 6]
#steps = [50]
#sizes = [5, 11, 15, 21, 25, 51] # size 21 fails. 19/6 fails. 
#sizes = [21, 31, 41, 51] # size 21 fails. 19/6 fails. 
#sizes = [61, 71, 81, 91, 101]
#sizes = [21, 41, 61, 81, 101]
#steps = [6]; sizes = [41]
steps = [50]; sizes = [51]
#sizes = [25, 51]
for size in sizes:
    for step in steps:
        mk_map(size=size, step=step)

print "All done."
