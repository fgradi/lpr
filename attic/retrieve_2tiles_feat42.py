#!/usr/bin/env python2.7
# coding: utf-8

# - read tifs
# - aggregate into tiles
# - write csv or pickle

# ## installation
# ### dependencies
# try apt-get install python-rasterio python-geopandas. On Ubuntu 16.04 this brings in rasterio-0.31
# 
# - install/upgrade via pip:
#     - pip install --upgrade enum34 requests rasterio geopandas
#  
# If pip install rasterio fails:
# rasterio/_base.c:287:22: fatal error: cpl_conv.h: No such file or directory
# just apt-get install libgdal-dev
# 
# 

# x read geotif. We have many.
# x read shp. Only one, but with many classes in them.
# x rasterise shp, use as filter for tif
# remove 0 label
# - decompose into cells, aggregate: compute histogram
# write aggregated CSV or pickle

# 1 working code
# 2 bugfree code
# 3 documented code
# 4 optimised code

# we want: features vs samples
# - train_df: only shp regions 
# - pred_df: everywhere

# list_of_tiles = create list of tiles. Should be standard operation for raster data.

# for the_tif in tifs:
#    read the_tif
#    for the_tile in list_of_tiles:
#        aggregate data on the_tile
# 


# In[ ]:


#get_ipython().magic(u'load_ext autoreload')
#get_ipython().magic(u'autoreload 2')

import geopandas
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats.mstats
from pdb import pm
import numpy.testing as npt
from sklearn import svm
from sklearn import tree
from sklearn import ensemble
from sklearn.model_selection import cross_val_score
from sklearn.cluster import KMeans
import string
from tictoc import Tic
import collections
import pub
from textwrap import dedent
from sklearn.metrics import confusion_matrix

from common import FeatureCallback, FeatureHistogram, Aggregator, \
                   path_to_landforms_shp, figsize, \
                   get_equal_freq_bins, interquatrile_range, BlockInfo, \
                   MapPlotter, map_class_ID_to_colour, \
                   load_src_and_band, map_step_to_scatter_size

# Plot style for Comp & Geosci
pub.SetPlotRC_CompGeosci()

import logging

logging.basicConfig(level=logging.WARN,
                    format='%(levelname)s:%(message)s')

# read land classes from shapefile
shapefile = geopandas.read_file(path_to_landforms_shp)
geoms = shapefile.geometry.values # list of shapely geometries
#geoms = geoms[0:3] # use just two classes
#geoms = [geoms[4], geoms[6], geoms[8]]
#geoms = [geoms[4], geoms[5], geoms[6],  geoms[7], geoms[8]]
#geoms = [geoms[4], geoms[8]]


# re-order land classes
#filter_geoms = range(len(geoms))           # do nothing
#filter_geoms = [0, 1, 2, 3, 4, 5, 6, 8]    # remove stirling ranges altogether
filter_geoms = [0, 1, 2, 3, 4, 5, 6, 8, 7] # move stirling ranges to back (note 8, 7)
geoms = [geoms[i] for i in filter_geoms]

# In[ ]:


relief_dict = {6: (46, 56, 66, 76), 
               5: (35, 45, 55, 65, 75, 34, 44, 54, 64, 74),
               3: (33, 43, 53, 63),
               2: (22, 32, 42, 52),
               1: (11, 21, 31, 41),
               4: (51, 61, 71, 62, 72, 73)}

def relief_histogram(data):
    """map relief. Accept flattened data, return a vector"""

    # map data to relief_class
    keys = [key if item in value else 0 for item in data for key, value in relief_dict.iteritems()]
    
    # compute histogram of relief class
    bins = [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5]
    return np.histogram(keys, bins=bins)[0]

def test_relief_histogram():
    npt.assert_array_equal(relief_histogram([11]), [1, 0, 0, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([22]), [0, 1, 0, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([33]), [0, 0, 1, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([51]), [0, 0, 0, 1, 0, 0])
    npt.assert_array_equal(relief_histogram([35]), [0, 0, 0, 0, 1, 0])
    npt.assert_array_equal(relief_histogram([46]), [0, 0, 0, 0, 0, 1])

test_relief_histogram()


# ## Assemble featureX dict

# In[ ]:


###############################################################################
## Assemble a dictionary of FeatureX classes, which describe potential features
## for our model, e.g. mrVBF divided into 10 bins.
## We assemble all possible features here, and later pick those which we 
## actually use for training.
###############################################################################

fdict = {}

def add_to_dict(ftif):
    ftif.add_to_dict(fdict)

if 1:
    the_tif = "elev.tif"
    add_to_dict(FeatureCallback(the_tif, ["eskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["ekurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["eiqr"], callback=interquatrile_range))

bins = np.arange(11)
add_to_dict(FeatureHistogram("mrvbf.tif", bins=bins, feature_prefix='vbf'))
add_to_dict(FeatureHistogram("mrrtf.tif", bins=bins, feature_prefix='rtf'))


# equal freq binning
if 1:
    the_tif = "profcurv.tif"
    n_bins = 5
    src, data, valid_data = load_src_and_band(the_tif)
    bins = get_equal_freq_bins(valid_data, n_bins)
    add_to_dict(FeatureHistogram(the_tif, bins=bins, feature_prefix='profcurv', feature_postfixes=np.arange(n_bins)+1))

##the_tif = "plancurv.tif"
##n_bins = 5
##src, data, valid_data = load_src_and_band(the_tif)
##bins = get_equal_freq_bins(valid_data, n_bins)
##add_to_dict(FeatureHistogram(the_tif, bins=bins, feature_prefix='plancurv', feature_postfix=np.arange(n_bins)+1)

the_tif = "twi.tif" # topo wetness index
add_to_dict(FeatureCallback(the_tif, ["tmean"], callback=np.mean))

if 1:
    the_tif = "slope.tif"
    add_to_dict(FeatureCallback(the_tif, ["sskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["skurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["siqr"], callback=interquatrile_range))

if 1:
    the_tif = "wei.tif" # wind exposure index
    add_to_dict(FeatureCallback(the_tif, ["wskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["wkurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["wiqr"], callback=interquatrile_range))
    add_to_dict(FeatureCallback(the_tif, ["wmean"], callback=np.mean))

# slopeRCL
if 1:
    # "slopeLE","slopeVG","slopeGE","slopeMO","slopeST","slopeVS",
    slope_bins  = [16, 26, 36, 46, 56, 66, 76]
    slope_names = ["LE", "VG", "GE", "MO", "ST", "VS", "CL"]
    bins=slope_bins
    #{"LE": 16, "VG": 26, "GE": 36, "MO": 46, "ST": 56, "VS": 66, "CL": 76}
    add_to_dict(FeatureHistogram("sloperelcl.tif", bins=slope_bins, feature_prefix='slope', feature_postfixes=slope_names[:-1]))

#"reliefP","reliefR","reliefL","reliefH","reliefM",
# relief
if 0:
    add_to_dict(FeatureCallback("sloperelcl.tif", feature_names=['relP', 'relR', 'relL', 'relH', 'relM', 'relB'], callback=relief_histogram))


## Pick which features to train on. Either use all features...
ftifs = [val for val in fdict.itervalues()] 

## ... or cherry-pick
#ftifs = [ fdict['vbf'] ]



# ## Aggregate training data

# In[ ]:

def mk_map(size, step, small_size):
    stats = collections.OrderedDict()
    ###############################################################################
    ## Aggregate training data into blocks
    ###############################################################################
    global session
    #import common
    #common.path_to_tifs = common.base + "tmp/AFO_s27_Landforms_2018_04_26"

    print "aggregate training ..."
    
    #step = 25; size = 91 # quick
    
    block_info = BlockInfo(size_x=size, size_y=size, step_x=step, step_y=step, 
                           small_size_x=small_size, small_size_y=small_size)
    #np.set_printoptions(precision=3, suppress=True)
    aggregator = Aggregator(block_info, ftifs, geoms)
    
    training_data = aggregator.aggregate(csv_filename=None, ax=None)
    stats["n_features"] = aggregator.n_features
    print "%i features: %s " % (aggregator.n_features, str(aggregator.hdr[:-3]))
    print "done."
    
    size_metres = np.array(aggregator.block_size_metres())
    
    print "size, step, small_size", size, step, small_size
    print "average block size %5.2f km" % (size_metres.mean() / 1000.)

    stats["size"] = size
    stats["step"] = step
    stats["small_size"] = small_size
    stats["avg_block_km"] = (size_metres.mean() / 1000.)
    
    ############################################################################### 
    ## Aggregate entire map, by passing geoms=None
    ###############################################################################
    print "aggregate entire map..."
    timer = Tic()
    aggregator_map = Aggregator(block_info, ftifs, geoms=None)
    entire_data = aggregator_map.aggregate(ax=None, csv_filename=None)
    stats["t_aggregate_map"] = timer.toc()
    print "feature matrix shape", entire_data.shape
    print "Done."
    
    ###############################################################################
    ## Train. Follow sklearn terminology: 
    ## X is matrix of training data, y is vector of labels
    ###############################################################################
    train_array = training_data[:,:-3]
    lonlat = training_data[:,-2:]

    # dict which maps clf name : (method, is_unsupervised)
    CLFs = {
            "lSV": (svm.LinearSVC(), False),
            "SV": (svm.SVC(), False),
            "DT": (tree.DecisionTreeClassifier(), False),
            "RF100" : (ensemble.RandomForestClassifier(n_estimators = 100, random_state = 42), False),
            "RF1000" : (ensemble.RandomForestClassifier(n_estimators = 1000, random_state = 42), False)
            }

    #CLFs = {"RF" : (ensemble.RandomForestClassifier(n_estimators = 100, random_state = 42), False)}
    
    #for n_clusters in range(2, 10):
    #    clf = KMeans(n_clusters=n_clusters, random_state=0) #.fit(X)
    #    clf_name = "km%i" % n_clusters
    
    for clf_name, (clf, is_unsupervised) in CLFs.iteritems():

        stats["clf_name"] = clf_name
        stats["is_unsupervised"] = is_unsupervised
    #clf = tree.DecisionTreeClassifier()
    #clf_name = "DT"
        plot_distance = False
        
        #clf = svm.SVC() #(kernel="poly", degree=3)
        #clf_name = "SVC"
        
        print "training...", clf_name
        X = training_data[:,:-3]
        y = training_data[:,-3]
        lonlat = training_data[:,-2:]
        
        ## k-fold cross-validation.
        ## k can't be greater than smallest sample size, so 
        ## count samples for each class
        k = 10
        hist, _ = np.histogram(y, bins=np.arange(0, len(geoms)))
        k = min(hist.min(), k)
        assert(hist.sum() == len(y))

        if k >= 2:
            print "%i-fold CV" % k
            stats["k-fold CV"] = k
            scores = cross_val_score(clf, X, y, cv=k)
            np.set_printoptions(precision=0)
            param_str = "size: %i  step: %i %s %i feat Acc: %0.3f (+/- %0.2f)\n%s" \
                % (size, step, clf_name, aggregator_map.n_features, 
                   scores.mean(), scores.std() * 2,
                string.join(["%3i" % int(item) for item in scores*1000]))

            stats["accuracy_mean"] = scores.mean()
            stats["accuracy_std"] = scores.std()
        else:
            param_str = "size: %i  step: %i %s %i feat no cross-valid possible" \
            % (size, step, clf_name, aggregator_map.n_features)
            stats["k-fold CV"] = -1
            stats["accuracy_mean"] = -1
            stats["accuracy_std"] = -1

        print param_str
        
        timer = Tic()
        if is_unsupervised:
            clf.fit(X)
        else:
            clf.fit(X, y)
        stats["t_train"] = timer.toc()

        print "done. "
        
        if 0:
            scatter_size = 5 # ... override anyway
            mp = MapPlotter(lonlat, scatter_size, geoms, map_class_ID_to_colour, plt.gca())
            mp.load_background_tif("mrvbf.tif")
            
            c_y = map_class_ID_to_colour(y)
            mp.plot(c_y, alpha=0.5)
        
        # there is a very nice plotting example 
        # http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
        ###############################################################################
        ## confusion matrix using entire training set (conceptually flawed!)
        ###############################################################################
        y_pred = clf.predict(X)
        cm = confusion_matrix(y, y_pred)
        cm_normed = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        np.set_printoptions(precision=2, floatmode='maxprec', suppress=True)
        print cm_normed
        
        ###############################################################################
        ## Predict entire map, then plot
        ###############################################################################
        X = entire_data[:,:-3]
        lonlat_map = entire_data[:,-2:]

        timer = Tic()
        predicted = clf.predict(X)
        stats["t_predict_map"] = timer.toc()
        
        ## plot final map. We use matplotlib's scatter plot. 
        ## scatter_size is fine-tuned such that we still get a somewhat seamless map.
        #map_step_to_scatter_size = {50: 880, 25: 220, 12:43, 6:8.4, 3:2}
        #map_step_to_scatter_size = {100: 2.4, 50: 0.6, 25: 0.15} # production ready figures
        #map_step_to_scatter_size = {200:31.1, 100: 7.8, 50: 1.95, 25: 0.4861} # linewidths=0
        scatter_size = map_step_to_scatter_size[step]

        for i in range(len(geoms)):
            print "%02i  %5i  %5i" % (i, (y == i).sum(), (predicted == i).sum())
            if (predicted == i).sum() == 0:
                logging.warn(dedent("""FIXME: this is where we should remove--or better:
                    flag--classes that our algo fails to predict"""))

        #scatter_size = 30.60 #0.4861 # ... override anyway
        # TEMP:
        quick=0
        if quick:
            mask = lonlat_map[:,1] > -27.5
            lonlat_map = lonlat_map[mask]
            predicted = predicted[mask]
        
        #predicted = np.random.randint(low=0, high=8, size=len(predicted))
        c_predicted = map_class_ID_to_colour(predicted)
        
        if plot_distance:
            # Returns distance to the decision boundary, for each class.
            # The decision boundary separates a class from the rest.
            distance = clf.decision_function(X)
        
            # If we have only two classes, decision_function returns a single vector,
            # in which negative values correspond to one side of the decision boundary, 
            # and positive to the other. Re-cast into a matrix for compatibility.
            if len(geoms) == 2:
                distance = np.vstack((-distance, distance)).transpose()
        
        fig, ax = plt.subplots(1,1,figsize=figsize)
        map_filename = session + "AFO_pred%s_size%03i_step%03i.png" % (clf_name, size, step)
        mp = MapPlotter(lonlat_map, scatter_size, geoms, map_class_ID_to_colour,
                        ax, aggregator.master_src, filename=None)
        mp.plot_background("background_mrvbf.tif")
        offset_px = (0.95*mp.src_image.width, 0.95*mp.src_image.height)
        mp.plot_block(size, offset_px)
        mp.plot_block(small_size, offset_px)

        ax.text(0., 1.05, param_str, transform=ax.transAxes)
        
        if clf_name.startswith("RF"):
            proba = clf.predict_proba(X)
            #thresholds = (0.3, 0.6, 0.9)
            #thresholds = (0.25, 0.5, 0.75, 0.9)
            #thresholds = np.linspace(0.1, 0.9, 9)
            mp.plot_proba(proba, thresholds=(0.5, 0.9), alpha=0.5)
            #mp.plot(c_predicted, alpha=0.5)
        
        elif 0 and (clf_name.startswith("SV") or clf_name.startswith("lSV")):
            # Returns distance to the decision boundary, for each class.
            # The decision boundary separates a class from the rest.
            distance = clf.decision_function(X)
        
            # If we have only two classes, decision_function returns a single vector,
            # in which negative values correspond to one side of the decision boundary, 
            # and positive to the other. Re-cast into a matrix for compatibility.
            if len(geoms) == 2:
                distance = np.vstack((-distance, distance)).transpose()
            mask_list = mp.plot_distance(predicted, distance, threshold=2., alpha=0.75)
        else:
            mp.plot(c_predicted, alpha=0.5)

        mp.plot_geoms()
        
        ax.legend(ncol=9, loc="lower left", 
             markerscale=0.02, columnspacing=0.5,
             handlelength=0.2, handletextpad=0.3, frameon=False, 
             bbox_to_anchor=(0.23, -0.029))
        
        plt.savefig(map_filename, dpi=200)
        plt.close()
        #mask_list = mp.plot_distance(predicted, distance, threshold=1.5, filename=map_filename, alpha=0.5)
        
            

        # add stats to dataframe
        global stats_df, stats_fname
        if stats_df is None:
            stats_df = pd.DataFrame([stats.values()], columns=stats.keys()) # note [] around values
        else:
            stats_df.loc[len(stats_df)] = stats.values()
        stats_df.to_csv(stats_fname, index=False)

    
# In[ ]:
steps = [25][::-1]
#sizes = np.arange(20, 421, 80)+1
sizes = (np.arange(50, 601, 50)+1) #[::-1]
session = "png/classesNoStirling_2tiles_feat42_noCrop/"

#steps = [100]; sizes = [601]; small_size=25; session = "png/tmp/" # quick testing 3"
#steps = [25]; sizes = [30]; small_size=25; session = "png/tmp/" # quick testing 3"
#steps = [25]; sizes = [91]; small_size=25; session = "png/tmp/" # quick on 27"

pub.mkdirs(session)
stats_fname = session + 'stats.csv'
try:
    stats_df = pd.read_csv(stats_fname)
except IOError:
    stats_df = None  # collect info about runs: classifier name, training time ...
#ex
#steps = [400, 200]; sizes = [11, 21]
#steps = [400]; sizes = [11]
#sizes = [25, 51]
for size in sizes:
    for step in steps:
        t_per_op = 1.5e-8
        n_op = (10000./step)**2 * size**2
        t_predicted = t_per_op * n_op
        print "t_predicted", t_predicted
        stats = mk_map(size=size, step=step, small_size=step)
print "All done."
