#!/bin/bash 
#SBATCH --time=04:00:00
#SBATCH --mem=80GB
#SBATCH --ntasks-per-node=1

set -u
#set -x  # print commands before executing
#set -e  # exit if single command fails


# load config
[ "$1" == "" ] && echo "usage: $0 <sessionfile>" && exit 1
SESSION=$1
set -u
. $SESSION
SESSION=`basename $SESSION`
echo "###### session $SESSION ######"


# non-temp and temp storage for large files
DATADIR="../../bigdata" # downloaded source data
TMPDIR="tmp"

#module load jdk
#module load R
#module load proj
#module load gdal
#module load geos
#module load python
#module load netcdf

#INFILE_ELEV_3="$DATADIR/3secSRTM_DEMs_v01/DEMS_ESRI_GRID_32bit_Float/dems3sv1_0/w001001.adf"
INFILE_ELEV_3="$DATADIR/3secSRTM_DEM/DEM_ESRI_GRID_16bit_Integer/dem3s_int/w001001.adf"
INFILE_SLOPE_3="$DATADIR/Slope/Slope_3_arc-second_resolution/Slope_degrees/mosaic/slopedeg3s.tif"
INFILE_MrRTF_3="$DATADIR/MrRTF_3_arc-second_resolution/mrrtf6g-a5_3s_median/mosaic/mrrtf_3s.tif"
INFILE_MrVBF_3="$DATADIR/MrVBF_3_arc-second_resolution/mrvbf6g-a5_3s_median/mosaic/mrvbf_3s.tif"
INFILE_PLANCURV_3="$DATADIR/PlanCurvature_3_arc-second_resolution/mosaic/plan_curvature_3s.tif"
INFILE_PROFCURV_3="$DATADIR/ProfileCurvature_3_arc-second_resolution/mosaic/profile_curvature_3s.tif"
INFILE_SLOPERELCL_3="$DATADIR/SlopeReliefClassification_3_arcsecond_resolution/mosaic/slope_relief_class_3s.tif"
INFILE_TWI_3="$DATADIR/TopographicWetnessIndex_3_arcsecond_resolution/mosaic/twi_3s.tif"
INFILE_WEI_3="$DATADIR/saga/WindExposition.sdat"
##INFILE_IP_3="$DATADIR/saga/IwahashiPike.sdat"
#SHAPEPATH="$DATADIR/Nacho1"
#SHAPENAME="Areas"
SHAPEPATH="$DATADIR/Landforms1"


mkdir -p "$SHAPEPATH"
mkdir -p "$TMPDIR" 

TMP_ELEV="$TMPDIR/elev.tif"
TMP_ELEV_9="$TMPDIR/elev9.tif"
TMP_SLOPE="$TMPDIR/slope.tif"
TMP_MrRTF="$TMPDIR/mrrtf.tif"
TMP_MrVBF="$TMPDIR/mrvbf.tif"
TMP_PLANCURV="$TMPDIR/plancurv.tif"
TMP_PROFCURV="$TMPDIR/profcurv.tif"
TMP_SLOPERELCL="$TMPDIR/sloperelcl.tif"
TMP_TWI="$TMPDIR/twi.tif"
TMP_ROUGHNESS="$TMPDIR/roughness.tif"
TMP_WEI="$TMPDIR/wei.tif"
TMP="$TMPDIR/tmp"
TMP_IP="$TMPDIR/ip.tif"
TMP_DOWNSAMPLED="$TMPDIR/tmp_downsampled.tif"

TRANSLATE_OPTS="-r bilinear -projwin $BBOX" # The default -r nearest is quicker, but can lead to 1-off errors 
#TRANSLATE_OPTS="$TRANSLATE_OPTS -a_nodata -127" # set nodata value. Mhh. That's funny:
# - gdalinfo says it's SIGNEDBYTE, min = -128, max = 8. But setting -a_nodata -128 produces a warning:
# Warning 1: for band 1, nodata value has been clamped to 0, the original value being out of range.
# Seems to be OK if -a_nodata is the ONLY task we give gdal_translate.
echo "OPTS <$TRANSLATE_OPTS>"


function clean {
    rm -f $TMP_ELEV
    rm -f $TMP_SLOPE
    rm -f $TMP_MrRTF
    rm -f $TMP_MrVBF
    rm -f $TMP_PLANCURV
    rm -f $TMP_PROFCURV
    rm -f $TMP_SLOPERELCL
    rm -f $TMP_TWI
    rm -f $TMP_WEI
    rm -f $TMP_IP
}

# show info only when verbose is set
function gdalinfo_wrapper () {
    [ $verbose = True ] || return
    gdalinfo -norat $*
}

function translate () {
    INFILE=$1
    OUTFILE=$2
    [ $verbose = True ] && echo "FILE $INFILE"
    gdalinfo_wrapper $INFILE
    gdal_translate $TRANSLATE_OPTS $INFILE $OUTFILE
    gdalinfo_wrapper $OUTFILE
}

if [ "$doTranslations" = True ] ; then
    #clean
    translate $INFILE_ELEV_3 $TMP_ELEV
    translate $INFILE_MrVBF_3 $TMP_MrVBF
    translate $INFILE_MrRTF_3 $TMP_MrRTF
    translate $INFILE_SLOPERELCL_3 $TMP_SLOPERELCL
    translate $INFILE_PLANCURV_3 $TMP_PLANCURV

    gdaldem roughness $TMP_ELEV $TMP_ROUGHNESS
    gdalinfo_wrapper -norat $TMP_ROUGHNESS
    #gdaldem color-relief $TMP_ROUGHNESS $DATADIR/Landscape/roughColours.txt $DATADIR/tmp/roughness_Coloured.png -of PNG

    translate $INFILE_PROFCURV_3 $TMP_PROFCURV
    translate $INFILE_TWI_3 $TMP_TWI
    translate $INFILE_SLOPE_3 $TMP_SLOPE

    saga_cmd ta_morphometry 27 -DEM $TMP_ELEV -EXPOSITION $TMP
    saga_cmd io_gdal 2 -GRIDS $TMP.sdat -FILE $TMP_WEI
    rm $TMP.*

    echo
    ls -latrh $TMPDIR
    echo

    # downsample and replace .tifs created above
    if [ "$doDownsample" = True ] ; then
        echo "###### Downsampling to $DOWNSAMPLE_RESOLUTION_DEGREES deg ######"
        rm -f $TMP_DOWNSAMPLED
        THE_TMPS="$TMP_ELEV $TMP_MrRTF $TMP_MrVBF $TMP_SLOPERELCL $TMP_SLOPE $TMP_ROUGHNESS
                  $TMP_PLANCURV $TMP_PROFCURV $TMP_TWI $TMP_WEI" # $TMP_IP

        for THE_TMP in $THE_TMPS; do
            gdalwarp -tr $DOWNSAMPLE_RESOLUTION_DEGREES $DOWNSAMPLE_RESOLUTION_DEGREES -r average $THE_TMP $TMP_DOWNSAMPLED 
            mv $TMP_DOWNSAMPLED $THE_TMP
            #exit
        done
    fi	
    
    # downsample MrVBF for background map
    gdalwarp -tr 0.002 0.002  -r average tmp/mrvbf.tif tmp/background_mrvbf.tif 


fi

echo "Finished"

#clean

