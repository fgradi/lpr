#!/usr/bin/env python3
import time


class Tic(object):
    """A very simple timer.
    
    timer = Tic()
    [do some work]
    print "it took", time.toc()
    """   
    def __init__(self, verbose=False):
        self.verbose = verbose
        self._t0 = time.time()

    def toc(self):
        self._t1 = time.time()
        duration = self._t1 - self._t0
        if self.verbose:
            print ("%g sec" % duration)
        return duration
