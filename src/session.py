# -*- coding: utf-8 -*-
"""
Config
The assigned values are default values. The Config files will overwrite them
Ludomotico contributed a cleaner version of read_from_file().
"""

from __future__ import print_function

import logging
import os
import sys
import traceback
import types
import numpy as np
from os.path import expanduser

# default_args_start # DO NOT MODIFY THIS LINE
# -*- coding: utf-8 -*-
# The preceding line sets encoding of this file to utf-8. Needed for non-ascii
# object names. It must stay on the first or second line.

# =============================================================================
FILTER_GEOMS = [0, 1, 2, 3, 4, 5, 6, 8]    # remove stirling ranges altogether
PLOT_DISTANCE = False
PLOT_BACKGROUND = True
CROSS_VALIDATE = True
CIRCULAR_MASK = True
USE_SMALL_BLOCKS = False
ATOL = 1e-9

STEPS = [25][::-1]
SIZES = (np.arange(50, 601, 50)+1)
SMALL_SIZE = 25
QUICK = False
C = 1.

HOME = expanduser("~")
BASE = os.path.join(HOME, "tmp/landscape/")
PATH_TO_LANDFORMS_SHP = BASE + "landforms/Landforms_2018_04_26.shp"
PATH_TO_TIFS = BASE + "tmp/AFO_s3tania_Landforms_2018_04_26"
PATH_TO_CACHE = BASE + "npz1/"

USE_CACHE_ONLY = False
# default_args_end # DO NOT MODIFY THIS LINE


def summary():
    """
    Return a string containing all parameters as key = value
    """
    s = []
    s.append('### Parameters ###')
    my_globals = globals()
    for k in sorted(my_globals.keys()):
        if k.startswith('__'):
            continue
        elif k == "args":
            continue
        elif k == "parser":
            continue
        elif isinstance(my_globals[k], type) or \
                isinstance(my_globals[k], types.FunctionType) or \
                isinstance(my_globals[k], types.ModuleType):
            continue
        else:
            s.append('%s = %s' % (k, my_globals[k]))
    s.append('###')
    return '\n'.join(s) + '\n'


def read_from_file(filename):
    logging.info('Reading parameters from file: %s' % filename)
    default_globals = globals()
    file_globals = dict()
    try:
        exec(compile(open(filename).read(), filename, 'exec'), file_globals)
    except IOError as reason:
        logging.error("Error processing file with parameters: %s", reason)
        sys.exit(1)
    except NameError:
        logging.error(traceback.format_exc())
        logging.error(f"Error while reading {filename}. Perhaps an unquoted string in your parameters file?")
        sys.exit(1)

    for k, v in file_globals.items():
        if k.startswith('_'):
            continue
        k = k.upper()
        if k in default_globals:
            default_globals[k] = v
        else:
            logging.warning('Unknown parameter: %s=%s' % (k, v))


def show_default():
    """show default parameters by printing all params defined above between
        # default_args_start and # default_args_end to screen.
    """
    f = open(sys.argv[0], 'r')
    do_print = False
    for line in f.readlines():
        if line.startswith('# default_args_start'):
            do_print = True
            continue
        elif line.startswith('# default_args_end'):
            return
        if do_print:
            print(line, end="")


def set_loglevel(args_loglevel=None):
    """Set loglevel from parameters or command line."""
    global LOGLEVEL
    if args_loglevel is not None:
        LOGLEVEL = args_loglevel
    LOGLEVEL = LOGLEVEL.upper()
    # -- add another log level VERBOSE. Use this when logging stuff in loops,
    #    potentially flooding the screen
    logging.VERBOSE = 5
    logging.addLevelName(logging.VERBOSE, "VERBOSE")
    logging.Logger.verbose = lambda inst, msg, *args, **kwargs: inst.log(logging.VERBOSE, msg, *args, **kwargs)
    logging.verbose = lambda msg, *args, **kwargs: logging.log(logging.VERBOSE, msg, *args, **kwargs)

    numeric_level = getattr(logging, LOGLEVEL, None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % LOGLEVEL)
    logging.basicConfig(level=numeric_level)
    logging.getLogger().setLevel(LOGLEVEL)


if __name__ == "__main__":
    pass
