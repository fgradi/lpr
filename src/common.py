#!/usr/bin/env python3

import rasterio
import rasterio.mask
import rasterio.plot
import os.path
import numpy as np
import numpy.testing as npt
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import shapely
import shapely.geometry
from affine import Affine
from pdb import pm
import logging
from textwrap import dedent
from hashlib import md5
import session
import errno

figsize = (5, 5)
_count = 0
earth_radius_m = 6371 * 1000.

# map class to color
_class_ID_to_cname = {
          -1: 'gray',
          0: 'red',
          1: 'yellow',
          2: 'dark_pink',
          3: 'dark_blue',
          4: 'light_blue',
          5: 'dark_green',
          6: 'light_green',
          7: 'beige',
          8: 'light_pink'}

_cname_to_colour = {
          'gray': '#888888',
          'red': '#b0171f',
          'yellow': '#ffdb00',
          'dark_pink': '#ca02bd',
          'dark_blue': '#3d59ab',
          'light_blue': '#13adf9',
          'dark_green': '#3f7543',
          'light_green': '#00cd66',
          'beige': '#b99d00',
          'light_pink': '#ff70ff'}

__class_colours = ['#888888',  # gray
                   '#b0171f',  # red
                   '#ffdb00',  # yellow
                   '#ca02bd',  # dark pink
                   '#3d59ab',  # dark blue
                   '#13adf9',  # light blue
                   '#3f7543',  # dark green
                   '#00cd66',  # light green
                   '#b99d00',  # beige
                   '#ff00ff']  # light pink


def mkdirs(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def hex_to_uint8(hex_colour):
    return tuple(bytes.fromhex(hex_colour[1:7]))


def map_class_ID_to_colour(class_ID):
    """map class_ID to colour code"""
    try:
        return _cname_to_colour[_class_ID_to_cname[class_ID]]
    except TypeError:
        return [_cname_to_colour[_class_ID_to_cname[item]] for item in class_ID]


def test_map_class_ID_to_colour():
    assert(map_class_ID_to_colour(8) == '#ff70ff')
    assert(map_class_ID_to_colour([0, 8]) == ['#b0171f', '#ff70ff'])


def memory_usage_MB():
    """Return current mem usage in MB as reported by ps"""
    # return the memory usage in MB
    MB = float(2**20)
    import psutil
    import os
    process = psutil.Process(os.getpid())
    return process.memory_info()[0] / MB


class BlockInfo(object):
    """Holds size and step of blocks, in pixels. Non-overlapping if step = size.

       step, size, and small_size can be either tuples (x, y), or scalars, in which case
       the same value is used for x and y.
    """
    def __init__(self, size, step, small_size=None):
        try:
            self.size_x = size[0]
            self.size_y = size[1]
        except TypeError:
            self.size_x = size
            self.size_y = size
        except IndexError:
            self.size_y = self.size_x

        try:
            self.step_x = step[0]
            self.step_y = step[1]
        except TypeError:
            self.step_x = step
            self.step_y = step
        except IndexError:
            self.step_y = self.step_x

        try:
            self.small_size_x = small_size[0]
            self.small_size_y = small_size[1]
        except TypeError:
            self.small_size_x = small_size
            self.small_size_y = small_size
        except IndexError:
            self.small_size_y = self.small_size_x


    def __str__(self):
        return "size%i,%i_step%i,%i_small%i,%i" % (
                self.size_x, self.size_y, self.step_x, self.step_y,
                self.small_size_x, self.small_size_y)


def plot_tif(src, band, ax):
    # plot
    # get extent from our GeoTIFF data. Plot band, which is a np.array
    extent = rasterio.plot.plotting_extent(src)
    ax.imshow(band, cmap=plt.cm.gray, extent=extent) # plot

    # Alternative 1: rasterio's built-in plotting. Automatically uses extent from src if given as (src, band-nr).
    #rasterio.plot.show((src, 1), with_bounds=True, cmap=plt.cm.gray, ax=ax)#, transform=src.transform)

    # Alternative 2: when plotting np.array, explicitly pass in extent.
    #rasterio.plot.show(band1, cmap=plt.cm.gray, ax=ax, extent=extent)


def plot_class(the_geom, ax):
    # plot shp
    x, y = the_geom.exterior.xy
    ax.plot(x, y)


def mask_extend(raster, shapes, extend_pixel, nodata=None, crop=False, all_touched=False,
                invert=False, crop_to_shape=True):
    """This reimplements rasterio.mask.mask, with two added features:
       1. Extend the bounding box of `shapes` by `extend_pixel`.

          rasterio.mask.mask returns a (rectangular) bounding box that tighly
          encloses `shapes`. If we want to extract a kernel which overlaps the
          `shapes` boundary, we need a band of additional data beyond `shape`.

       2. `crop_to_shape` controls whether or not to crop extended bounding box
          to the actual shapes.

          If False, return a matrix matching the extended bounding box, with
          all valid data.

          If True (Default), return the same matrix but mask out locations
          outside `shapes`. This matches the behaviour of rasterio.mask.mask().

          Note that `crop_to_shape` is different from `crop`, which controls
          whether or not the result is cropped to bbox, or returned in the shape
          of the entire map.
    """
    if len(extend_pixel) != 2:
        raise ValueError("extend_pixel must have length of 2.")

    from rasterio.features import geometry_mask
    if crop and invert:
        raise ValueError("crop and invert cannot both be True.")
    if nodata is None:
        if raster.nodata is not None:
            nodata = raster.nodata
        else:
            nodata = 0

    all_bounds = [rasterio.features.bounds(shape) for shape in shapes]
    minxs, minys, maxxs, maxys = zip(*all_bounds)
    mask_bounds = (min(minxs), min(minys), max(maxxs), max(maxys))

    try:
        # rasterio v0.36
        invert_y = raster.affine.e > 0
        rasterio_pre_v1 = True
    except AttributeError:
        # rasterio v1.0
        invert_y = raster.transform.e > 0
        rasterio_pre_v1 = False
    source_bounds = raster.bounds
    if invert_y:
        source_bounds = [source_bounds[0], source_bounds[3],
                         source_bounds[2], source_bounds[1]]
    if rasterio.coords.disjoint_bounds(source_bounds, mask_bounds):
        if crop:
            raise ValueError("Input shapes do not overlap raster.")
        else:
            logging.warn("GeoJSON outside bounds of existing output raster."
                         "Are they in different coordinate reference systems?")

    # extend. Work out lon from px
    if rasterio_pre_v1:
        dlon = extend_pixel[0] * raster.transform[1]
        dlat = -extend_pixel[1] * raster.transform[5]
    else:
        dlon = extend_pixel[0] * raster.transform[0]
        dlat = -extend_pixel[1] * raster.transform[4]

    mask_bounds = (mask_bounds[0]-dlon, mask_bounds[1]-dlat, mask_bounds[2]+dlon, mask_bounds[3]+dlat)

    if invert_y:
        mask_bounds = [mask_bounds[0], mask_bounds[3],
                       mask_bounds[2], mask_bounds[1]]
    if crop:
        window = raster.window(*mask_bounds)
        out_transform = raster.window_transform(window)

    else:
        window = None
        out_transform = raster.transform

    out_image = raster.read(window=window, masked=True)
    out_shape = out_image.shape[1:]

    shape_mask = geometry_mask(shapes, transform=out_transform, invert=invert,
                               out_shape=out_shape, all_touched=all_touched)
    out_image.mask = out_image.mask | shape_mask
    out_image.fill_value = nodata

    if crop_to_shape:
        for i in range(raster.count):
            out_image[i] = out_image[i].filled(nodata)

    return out_image, out_transform


# https://stackoverflow.com/questions/16856788/slice-2d-array-into-smaller-2d-arrays
# UNUSED
def blockshaped(arr, nrows, ncols):
    """
    Return an array of shape (n, nrows, ncols) where
    n * nrows * ncols = arr.size

    If arr is a 2D array, the returned array should look like n subblocks with
    each subblock preserving the "physical" layout of arr.
    """
    h, w = arr.shape
    return (arr.reshape(h//nrows, nrows, -1, ncols)
               .swapaxes(1, 2)
               .reshape(-1, nrows, ncols))


class Aggregator(object):
    """Extract data confined within a landform class. Subdivide into overlapping blocks. Compute features for each block.

    :param block_info: A BlockInfo instance, containing size and step for a block
    :param ftifs: list of FeatureX instances, describing which/how features are computed
    :param geoms: list of shapely geometries
    :param global_nodata: scalar used to indicate invalid data, defaults to 1e99

    Our tif::

        +-------------------------------------
        |  ......................
        |  .                    .
        |  .      *****         .     ****  landform class boundary (aka geom)
        |  . .**** |  |******   .
        |  . *--+--+--+--+--+*  .     +--+
        |  . *  |  |  |  |  | * .     |  |  a block ('tile' in our paper)
        |  . *--+--+--+--+--+-* .     +--+
        |  .  ***  |  |  |  **  .
        |  .    ***********     .     ...   extended bounding box of geom
        |  . ....................
        |

    1. Starting with one big matrix (eg elevation), extract data confined
       within the extended bounding box of a labeled (training) landform class boundary.
       See docs/comment_extBB.png as to why we need to extend the bounding box.
    2. Subdivide into (overlapping) blocks, each of which will create one data point.
    3. For each block, compute a number of features.
    4. Assemble feature matrix (features vs data points)
    5. Repeat for each landform class

    The above steps are done by a calling our main worker method aggregate():
    Step 1.    done in get_data_bounded_by_geom(), called by _mk_list_of_columns()
         2.,3. done in _aggregate()
         4.    done in assemble_feature_matrix()

    Final data structure (aka feature matrix):

    Assume we

    - have 3 landform classes
    - compute 7 features from tif A (e.g, elev0..6 from elev.tif)
    - and 3 from tif B (e.g. slope0..2 from slope.tif),

    our final matrix will look like so::

                 features (f0 .. fX) -->

                 This
                 column
                 is
                 from     from
                 tif A    tif B

                f0 .. f6 f7 ..f9 label lon lat
                +-------+-------+-----+---+---+
                |the_mat|the_mat|     |   |   |
      data  |   | class | class |     |   |   |
      points|   |   1   |   1   |
            v   +-------+-------+-
                |the_mat|       |
                | class |
                |   2   |
                +-------+-
                |the_mat|
                | class |
                |   3   |
                +-------+-

                    ^
                    |
                tif_column


    Which and how features are computed is controlled by a list of FeatureX() objects.

    - load a tif. Extract the_matrix for each class
    - put all matrices into a list called tif_column
    - repeat for all tifs. The list of tif_columns is called list_of_columns, which
       will produce our final_matrix
    """
    def __init__(self, block_info, ftifs, geoms=None, global_nodata=1e99,
                 min_valid_pixel_ratio=0.25, circular_mask=False,
                 use_small_blocks=False):
        np.random.seed(0)
        self.block_info = block_info
        self.global_nodata = global_nodata
        self.ftifs = ftifs
        self.feature_matrix = None
        self.use_small_blocks = use_small_blocks
        self.hdr, self.fmt, self.n_features = self._mk_hdr_and_fmt(ftifs)

        if circular_mask:
            self.circular_mask = self.mk_circular_mask(self.block_info.size_x,
                                                       self.block_info.size_y,
                                                       self.block_info.size_x/2)
            if use_small_blocks:
                self.small_circular_mask = self.mk_circular_mask(self.block_info.small_size_x,
                                                                 self.block_info.small_size_y,
                                                                 self.block_info.small_size_x/2)
        else:
            self.circular_mask = False
            self.small_circular_mask = False

        # load first tif. All other tifs will need to match this one in size, resolution etc
        filename = self.ftifs[0].filename
        logging.info("Loading master tif " + filename)
        self.master_src = rasterio.open(filename)

        if ftifs is not None and (geoms is None):
            logging.info("auto-gen geoms from master tif")
            # read tif and print stats
            b = self.master_src.bounds
            coords = ((b.left, b.top), (b.right, b.top), (b.right, b.bottom), (b.left, b.bottom))
            polygon = shapely.geometry.Polygon(coords)
            geoms = [polygon]

        if type(geoms) not in (list, np.ndarray):
            raise TypeError("list or ndarray expected for geoms")
        self.geoms = self._mark_invalid_geoms(geoms)

        self.seen_class_IDs = set()
        self.list_of_columns = []
        self.cords_column = []  # list of (X,Y) tuples, per class

        self._check_ftifs()
        self.n_data = self._count_data_points(ftifs[0].filename)
        self.min_valid_pixel_ratio = min_valid_pixel_ratio
        self._max_invalid_pixel_count = int((1-self.min_valid_pixel_ratio) * self.block_info.size_y
                                            * self.block_info.size_y)

    def mk_circular_mask(self, size_x, size_y, r):
        """return a mask of [`size_y`, `size_x`] with a centred circular mask of radius `r`"""
        y, x = np.ogrid[-size_y/2+1:size_y/2+1, -size_x/2+1:size_x/2+1]
        return x*x + y*y > r*r

    def shape(self):
        """Returns the shape of the tile matrix."""
        irange, jrange = self._ijrange(self.master_src, self.block_info)
        return (len(irange), len(jrange))

    def valid_geoms_generator(self, geoms):
        """Yield non-None geoms"""
        for class_ID, the_geom in enumerate(geoms):
            if the_geom is not None:
                yield class_ID, the_geom

    def block_size_metres(self):
        """return the size of a block, in metres"""
        src_deg_per_px = np.array([(self.master_src.bounds.right-self.master_src.bounds.left) / self.master_src.width,
                                   (self.master_src.bounds.top-self.master_src.bounds.bottom) / self.master_src.height])

        # metres per degree at our src data's mean latitude
        avg_lat = (self.master_src.bounds.top + self.master_src.bounds.bottom) / 2.
        m_per_deg = 2 * np.pi * earth_radius_m / 360. * np.array([np.cos(np.deg2rad(avg_lat)), 1.])

        # metres per pixel
        m_per_px = m_per_deg * src_deg_per_px
        return self.block_info.size_x * m_per_px[0], self.block_info.size_y * m_per_px[1]

    def get_data_bounded_by_geom(self, src, the_geom):
        """extract raster values within the class polygon"""
        # get our classes' shapely polygon and transform to GeJSON format
        class_JSON = [shapely.geometry.mapping(the_geom)]

        # Extend the bounding box by half our block size.
        # See docs/comment_extBB.png as to why we need to extend the bounding box.
        extend_pixels = (self.block_info.size_x/2, self.block_info.size_y/2)

        # Funny. This returns a masked array (ma), but mask is all False?!
        cropped_ma, cropped_transform = mask_extend(src, class_JSON, extend_pixels,
                                                    crop=True, crop_to_shape=False)
        # out_image = out_image.squeeze() # if src is single-band, remove useless dimension

        return cropped_ma, cropped_transform

    def aggregate(self, ax=None, csv_filename=None, delimiter=',',
                  path_to_cache=None):
        """main worker: Aggregate data

        If path_to_cache is given, feature matrix is cached."""
        self.ax = ax
        feature_matrix = None

        # Try loading cache first...
        if path_to_cache is not None:
            # hash should include stuff that is specific to our aggregation
            # circular mask is either boolean or array of bool, so use np.any() to test
            str_to_hash = (" ".join(self.hdr) + str(np.any(self.circular_mask)) + str(self.use_small_blocks))
            logging.info("str_to_hash <%s>", str_to_hash)
            hash_ = md5(str_to_hash.encode())
            npz_filename = os.path.join(path_to_cache, "%s_%s_feat%i.npz" % (
                    hash_.hexdigest(), str(self.block_info), len(self.hdr)))

            logging.info("Loading cache %s" % npz_filename)
            try:
                npz_file = np.load(npz_filename)
                feature_matrix = npz_file['feature_matrix']
                logging.info("OK.")
            except IOError:
                logging.info("failed.")
                if "train" not in path_to_cache and session.USE_CACHE_ONLY:
                    raise ValueError("Cache not found.")

        # ... failing that, aggregate.
        if feature_matrix is None:
            self._mk_list_of_columns()
            feature_matrix = self._assemble_feature_matrix(csv_filename=csv_filename, ax=ax, delimiter=delimiter)
            if path_to_cache is not None:
                # write summary to file, then write cache
                summary_filename = os.path.join(path_to_cache, "%s.summary.txt" % (
                        hash_.hexdigest()))
                with open(summary_filename, "w") as f:
                    f.write(session.summary())
                    f.write("hdr = %s\n" % self.hdr)

                np.savez(npz_filename, feature_matrix=feature_matrix)

        logging.info("%i features: %s ", self.n_features, str(self.hdr[:-3]))
        logging.info("feature matrix shape %s", str(feature_matrix.shape))

        return feature_matrix

    def _ijrange(self, data, block_info):
        """list of upper-left locations of blocks"""
        irange = np.arange(0, data.shape[0] - block_info.size_y + 1, block_info.step_y)
        jrange = np.arange(0, data.shape[1] - block_info.size_x + 1, block_info.step_x)
        return irange, jrange

    def _mark_invalid_geoms(self, geoms):
        """replace invalid geoms (ie those that do not overlap our tif) with None"""
        marked_geoms = []
        tif_filename = self.ftifs[0].filename
        src, _, _ = load_src_and_band(tif_filename)
        for class_ID, the_geom in enumerate(geoms):
            try:
                cropped_ma, cropped_transform = self.get_data_bounded_by_geom(src, the_geom)
            except ValueError as e:
                if e.message != 'Input shapes do not overlap raster.':
                    raise e
                logging.warn("Skipping landclass #%i as it does not overlap %s" % (class_ID, tif_filename))
                the_geom = None

            marked_geoms.append(the_geom)
        return marked_geoms

    def _check_ftifs(self):
        """Check that resolution etc match with that of master tif"""
        error_flag = False
        for the_ftif in self.ftifs[1:]:
            filename = the_ftif.filename
            src = rasterio.open(filename)

            # check size and resolution
            if src.shape != self.master_src.shape:
                logging.error("Shape of %s doesn't match master" % filename)
                error_flag = True
            if not np.allclose(np.array(src.transform), np.array(self.master_src.transform),
                               atol=session.ATOL, rtol=0.):
                logging.error("Transform of %s doesn't match master.\n"
                              "Try gdal_translate -r bilinear ... when creating tif.""" % filename)
                error_flag = True

        if error_flag:
            raise ValueError("Errors while checking tifs. See error message(s) above.")

    def _count_data_points(self, the_tif):
        """count number of data points enclosed by geoms. This is used later
           for validation
        """
        n_data = 0
        band_ID = 0
        for class_ID, the_geom in self.valid_geoms_generator(self.geoms):
            cropped_ma, cropped_transform = self.get_data_bounded_by_geom(self.master_src, the_geom)
            cropped_band = cropped_ma[band_ID]
            irange, jrange = self._ijrange(cropped_band, self.block_info)
            logging.info("scanning %i %i" % (len(irange), len(jrange)))
            n_data += len(irange) * len(jrange)

        logging.info("COUNT n_data %i" % n_data)
        assert(n_data > 0)
        return n_data

    def full_lat_lon_for_class(self, data, cropped_transform):
        T1 = cropped_transform * Affine.translation(0.5, 0.5)
        R, C = np.meshgrid(np.arange(data.shape[0]), np.arange(data.shape[1]))
        # FIXME: this works, but I'm not entirely clear about row/col order here.
        X, Y = ((R, C) * T1)
        return X, Y

    def store_lat_lon_for_class(self, class_ID, irange, jrange, cropped_transform):
        """store LatLon for this landclass. Is used later when assembling final matrix"""
        if class_ID not in self.seen_class_IDs:
            self.seen_class_IDs.add(class_ID)

            # https://stackoverflow.com/questions/27861197/how-to-i-get-the-coordinates-of-a-cell-in-a-geotif
            # The convention for the transform array as used by GDAL (T0) is
            # to reference the pixel corner. You may want to instead reference
            # the pixel centre, so it needs to be translated by 50%:
            T1 = cropped_transform * Affine.translation(0.5, 0.5)

            RC = np.zeros((len(irange), len(jrange), 2))
            # FIXME: list comprehension
            for i, i0 in enumerate(irange):
                for j, j0 in enumerate(jrange):
                    # i,j are top-left corners, but we want lat/lon for centre.
                    r = i0 + self.block_info.size_y / 2.
                    c = j0 + self.block_info.size_x / 2.
                    RC[i, j] = (r, c)

            # FIXME: this works, but I'm not entirely clear about row/col order here.
            X, Y = ((RC[:, :, 1], RC[:, :, 0]) * T1)

            # loses geom info
            self.cords_column.append((X.ravel(), Y.ravel()))

        else:
            pass
            # check if XY match

    # FIXME: if speed becomes a concern, use a mask everywhere.
    #        Could parallelise over tifs.
    def _mk_hdr_and_fmt(self, ftifs):
        """Assemble header and format string"""
        hdr = []
        fmt = []
        for the_ftif in ftifs:
            hdr += the_ftif.feature_names
            fmt += ["%7.5f"] * len(the_ftif.feature_names)

        if self.use_small_blocks:
            for the_ftif in ftifs:
                hdr += ['S' + item for item in the_ftif.feature_names]
                fmt += ["%7.5f"] * len(the_ftif.feature_names)

        n_features = len(hdr)  # dont' count label, lat, lon

        return hdr, fmt, n_features

    def _assemble_feature_matrix(self, csv_filename=None, delimiter=',',
                                 ax=None):
        """Assemble and return final_matrix from self.list_of_columns. Add lon,
           lat, label. Write to CSV if so instructed.

        :param csv_filename: Filename to write data to. Skip writing if None
                             (default)
        :param delimiter: CSV delimiter
        :param ax: axis to plot against, mostly for debugging.
        """

        if csv_filename is not None:
            outf = open(csv_filename, "a")
            logging.info("Writing to CSV %s", csv_filename)

        logging.info("Final: %i features: %s" % (self.n_features,
                                                 str(self.hdr)))

        hdr = delimiter.join(self.hdr + ["label", "lat", "lon"])
        fmt = self.fmt + ["%i", "%7.5f", "%7.5f"]

        rows = []

        for i, (class_ID, _) in enumerate(self.valid_geoms_generator(self.geoms)):

            the_row = [self.list_of_columns[j][i] for j in range(len(self.ftifs))]

            # append class label
            class_label = np.zeros((the_row[0].shape[0], 1)) + class_ID

            the_row.append(class_label)
            the_row.append(self.cords_column[i][0].reshape(-1, 1))
            the_row.append(self.cords_column[i][1].reshape(-1, 1))

            # make_the row a np.array
            the_row = np.hstack(the_row)

            # mask row if any feature is nodata.
            mask = (the_row != self.global_nodata).all(axis=1)

            # remove masked values
            the_row = the_row[mask]
            rows.append(the_row)

            # plot coords
            if ax is not None:
                ax.plot(the_row[:, -2], the_row[:, -1], '.')

            # write to csv. or pickle
            if csv_filename is not None:
                np.savetxt(outf, the_row, fmt=delimiter.join(fmt),
                           header=hdr, delimiter=delimiter)
            hdr = ""  # write hdr only once

        if csv_filename is not None:
            outf.close()

        final = np.vstack(rows)
        logging.info("final " + str(final.shape))
        return final

    def _mk_list_of_columns(self):
        """For every labeled landclass, crop a bbox matrix from the_tif, then
           aggregate features.

           Loop over all tifs and all labeled landclasses. Extract a rectangular bbox.
        """
        self.list_of_columns = []
        band_ID = 0  # FIXME: perhaps this should be provided as an argument?
        for the_ftif in self.ftifs:
            logging.info(the_ftif.filename)

            # read tif and print stats
            src = rasterio.open(the_ftif.filename)
            logging.info("Nodata value %s\n"
                         "  count %i\n"
                         "  size %i x %i\n"
                         "  indexes (bands) %s\n" % (str(src.nodata), src.count,
                                                     src.width, src.height, str(src.indexes)))
            # plot tif
            if self.ax is not None:
                band1 = src.read(band_ID+1)  # band_ID starts at 1
                band1[band1 == src.nodata] = 15
                plot_tif(src, band1, self.ax)

            # list of matrices [i data points, j features], one per class
            tif_column = []

            n_ftif_features = len(the_ftif.feature_names)
            if self.use_small_blocks:
                n_ftif_features *= 2  # because we have small and large tiles

            # loop classes
            for class_ID, the_geom in self.valid_geoms_generator(self.geoms):

                # this extracts the chunk of data bounded by our geom class
                cropped_ma, cropped_transform = self.get_data_bounded_by_geom(src, the_geom)

                cropped_band = cropped_ma[band_ID]
                irange, jrange = self._ijrange(cropped_band, self.block_info)

                self.store_lat_lon_for_class(class_ID, irange, jrange, cropped_transform)

                if self.ax is not None:
                    plot_class(the_geom, ax=self.ax)

                if class_ID == 0:
                    logging.info(the_ftif.feature_names)

                # returns array of (datapoints, features) for current class.
                # We will eventually Vstack other classes, then Hstack other features.
                the_matrix = self._aggregate2tiles(cropped_band, the_ftif, src.nodata)

                # loses geom info. This is now a matrix [data points, features]
                the_matrix = the_matrix.reshape(-1, n_ftif_features)
                tif_column.append(the_matrix)

            self.list_of_columns.append(tif_column)

            # What remaining lines just test shape of data

            logging.info("  expecting %i data" % self.n_data)

            count = 0
            for the_matrix in tif_column:
                count += the_matrix.shape[0]
                logging.debug("shape %s, sum=%i" % (the_matrix.shape, count))
            logging.debug("sum", count, "should be", self.n_data)

            # this test if we still had image shape...
            #assert(n_data == sum([the_matrix.shape[0]*the_matrix.shape[1] for the_matrix in tif_column]))

            # ... but after ravel(), use this:
            assert(self.n_data == sum([the_matrix.shape[0] for the_matrix in tif_column]))

    def check_features(self, features, block):
        if not np.isfinite(features).all():
            print("uh-oh. features:")
            np.set_printoptions(precision=2, suppress=True)
            print(features)
            print(block)
            print(block.data)
            raise ValueError("NaNs in here. Oh dear.")

    def invalid_mask(self):
        """Return mask

        Length should match first dimension of feature matrix
        """
        data = self.master_src.read(1)
        nodata = self.master_src.nodata
        b = self.block_info
        mask = data[b.size_y/2:data.shape[0] - b.size_y/2:b.step_y,
                    b.size_x/2:data.shape[1] - b.size_x/2:b.step_x] == nodata

        assert(len(mask.ravel()) == self.n_data)

        return mask

    def _aggregate(self, data, the_ftif, nodata):
        """Return array of feature vectors for data.

        Subdivide data into tiles of given tile_size_x (and _y).
        Overlap can be controlled by step_size_x, i.e., non-overlapping if tile_size = step_size

        For each tile, call the compute() method of the_ftif.

        :param data: a np array
        :param the_ftif: a FeatureX instance
        :nodata: scalar indicating invalid data

        FIXME: dumb first implementation.
        Perhaps be more efficient with numpy strides
        https://docs.scipy.org/doc/numpy-1.14.0/reference/generated/numpy.ndarray.strides.html
        """
        # How to deal with partially masked tiles?
        # 1. ignore tiles that are (partially) masked. Simple, but reduces available training data.
        # 2. include only unmasked pixels in histogram, adjust block.size accordingly?
        #    This might skew our feature.
        #
        # Go for 1.
        irange, jrange = self._ijrange(data, self.block_info)
        features = np.zeros((len(irange), len(jrange), len(the_ftif.feature_names)))

        for i, i0 in enumerate(irange):
            for j, j0 in enumerate(jrange):
                block = data[i0:i0+self.block_info.size_y, j0:j0+self.block_info.size_x]

                # make mask
                block = np.ma.masked_array(block, block==nodata)  # True indicates masked out values

                if block.mask.sum() > self._max_invalid_pixel_count:
                    block_features = self.global_nodata
                else:
                    block_features = the_ftif.compute(block)
                    if not np.isfinite(block_features).all():
                        print("uh-oh. features:")
                        np.set_printoptions(precision=2, suppress=True)
                        print(block_features)
                        print("masked block")
                        print(block)
                        print(block.data)

                        raise ValueError("NaNs in here. Oh dear.")

                # FIXME: replace count by enumerate and meshgrid
                features[i, j] = block_features  # this keeps spatial information.

        return features

    def _aggregate2tiles(self, data, the_ftif, nodata):
            """Return array of feature vectors for data.

            # aggregate 2 types of tiles: block and small_block.
            # Idea: small tiles are a subset of larger ones.
            # x do usual procedure for large tile
            # x then aggregate small tile in the middle
            # ! - overlap will be potentially huge for large tiles, but so what.
            # step = small_size
            # This will double the number of features.
            #

            Subdivide data into tiles of given tile_size_x (and _y).
            Overlap can be controlled by step_size_x, i.e., non-overlapping if tile_size = step_size

            For each tile, call the compute() method of the_ftif.

            :param data: a np array
            :param the_ftif: a FeatureX instance
            :nodata: scalar indicating invalid data

            FIXME: dumb first implementation.
            Probably be more efficient with numpy strides
            https://docs.scipy.org/doc/numpy-1.14.0/reference/generated/numpy.ndarray.strides.html
            """
            # How to deal with partially masked tiles?
            # 1. ignore tiles that are (partially) masked. Simple, but reduces available training data.
            # 2. include only unmasked pixels in histogram, adjust block.size accordingly?
            #    This might skew our feature.
            #
            # Go for 1.
            irange, jrange = self._ijrange(data, self.block_info)

            n_ftif_features = len(the_ftif.feature_names)  # number of features in this ftif
            n = 2 * n_ftif_features if self.use_small_blocks else n_ftif_features
            features = np.zeros((len(irange), len(jrange), n))

            for i, i0 in enumerate(irange):
                for j, j0 in enumerate(jrange):
                    block = data[i0:i0+self.block_info.size_y, j0:j0+self.block_info.size_x]

                    # make mask
                    block = np.ma.masked_array(block, block == nodata)  # mask is True for masked values

                    # FIXME: that count should include circular mask, or not?
                    if block.mask.sum() > self._max_invalid_pixel_count:
                        features[i, j] = self.global_nodata
                    else:
                        # apply circle here
                        block.mask = block.mask | self.circular_mask

                        # compressing returns only non-masked data. This
                        # ensures that eg. np.hist works properly.
                        # However, this flattens our array, so spatial info is lost
                        block_features = the_ftif.compute(block.compressed())
                        features[i, j, :n_ftif_features] = block_features

                        if not self.use_small_blocks:
                            continue

                        # repeat for small tile. 's' is for 'small'
                        si0 = (self.block_info.size_y - self.block_info.small_size_y) / 2
                        sj0 = (self.block_info.size_x - self.block_info.small_size_x) / 2

                        small_block = block[si0:si0+self.block_info.small_size_y,
                                            sj0:sj0+self.block_info.small_size_x]
                        # NB: this overwrites block.mask
                        small_block.mask = small_block.mask | self.small_circular_mask

                        # after applying mask, do we still have enough valid data?
                        compressed = small_block.compressed()
                        # FIXME: could use similar max invalid count as for block above
                        if len(compressed) == 0:
                            features[i, j] = self.global_nodata
                        else:
                            small_block_features = the_ftif.compute(compressed)
                            # FIXME: replace count by enumerate and meshgrid
                            features[i, j, n_ftif_features:] = small_block_features  # this keeps spatial information.

            return features


def test_aggregate_histogram():
    data = (np.arange(4*5) % 11).reshape(4, 5)
    block_info = BlockInfo(size_x=3, size_y=2, step_x=1, step_y=2)
    bins = np.linspace(0, 10, 11)
    ftif = FeatureHistogram("mrvbf.tif", bins=bins, feature_prefix='vbf')
    aggregator = Aggregator(block_info, [ftif], None)
    actual = aggregator._aggregate(data, ftif, nodata=255).reshape(2, 3, 10)

    desired = np.fromstring("""
          1.  1.  1.  0.  0.  1.  1.  1.  0.  0.
          0.  1.  1.  1.  0.  0.  1.  1.  1.  0.
          0.  0.  1.  1.  1.  0.  0.  1.  1.  1.
          1.  1.  0.  0.  1.  1.  1.  0.  0.  1.
          1.  1.  1.  0.  0.  1.  1.  1.  0.  0.
          0.  1.  1.  1.  0.  0.  1.  1.  1.  0.
        """, dtype=float, sep=' ').reshape(2, 3, 10) / 6.  # div by 6 to account for block_size

    npt.assert_almost_equal(actual, desired, decimal=14)


def test_aggregate_callback():
    data = (np.arange(4*5) % 11).reshape(4, 5)
    ftif = FeatureCallback("mrvbf.tif", ["tmean"], callback=np.mean)
    block_info = BlockInfo(size_x=3, size_y=2, step_x=1, step_y=2)
    aggregator = Aggregator(block_info, [ftif], None)
    actual = aggregator._aggregate(data, ftif, nodata=255)
    desired = np.array([3.5, 4.5, 5.5, 13/3., 3.5, 4.5]).reshape(2, 3, 1)
    npt.assert_almost_equal(actual, desired, decimal=14)


def load_src_and_band(tif_filename, band_ID=1):
    src = rasterio.open(tif_filename)
    data = src.read(band_ID)
    valid_data = data[data != src.nodata]
    return src, data, valid_data


def inspect_tif_band(the_ftif, band_ID=None, bins=None):
    """Print some info about a given band"""
    src, data, valid_data = load_src_and_band(the_ftif.filename)  # read the tif
    print(the_ftif)
    print("  Nodata value", src.nodata)
    print("  count", src.count)
    print("  size %i x % i" % (src.width, src.height))
    print("  indexes (bands)", src.indexes)

    if bins is None:
        print("using ftif bins")
        bins = the_ftif.bins
    else:
        print("overriding bins")

    print("data shape", data.shape)
    valid_data = data[data != src.nodata]
    hist = plt.hist(valid_data.ravel(), bins=bins)
    print("valid min/max", valid_data.min(), valid_data.max())
    return valid_data, hist


def get_equal_freq_bins(data, n_bins):
    """return bins for equal frequency binning of data into n_bins."""
    percentiles = [per for per in np.linspace(0, 100, n_bins+1)[:]]
    bins = np.percentile(data, percentiles)
    return bins


# FIXME: should make virtual base class FeatureX
# FeatureX: desribes how to compute features for given tif.

class FeatureHistogram(object):
    """Compute histogram using given bins. Each bin (normalised) will be a feature.

       Feature names are created by concatenating feature_prefix + feature_postfix.

       :param filename: the tif filename
       :param bins: bins to be used for histogram
       :feature_prefix: a string used as prefix for feature names
       :feature_postfixes: a list of post fixes, one per feature
    """
    def __init__(self, filename, bins, feature_prefix, feature_postfixes=None):
        self.filename = filename
        self.bins = bins
        self.feature_prefix = feature_prefix
        # if no postfix given, use bins
        if feature_postfixes is None:
            self.feature_names = [feature_prefix + str(i) for i in bins[:-1]]
        else:
            self.feature_names = [feature_prefix + str(item) for item in feature_postfixes]
        assert(len(bins) == len(self.feature_names) + 1)

    def add_to_dict(self, the_dict):
        """Create a key add self to dictionary"""
        key = "".join(self.feature_prefix)
        the_dict[key] = self

    def __str__(self):
        return dedent("""
        %s
        bins %s
        %s-%s
        """) % (self.filename, str(self.bins), self.feature_prefix, self.feature_names)

    def compute(self, block):
        """Accept :ndarray: block, compute histogram using bins, normalised by number of elements in block."""
        return np.histogram(block.ravel(), self.bins)[0] / float(block.size)


class FeatureCallback(object):
    """A container for a tif and associated features"""
    def __init__(self, filename, feature_names, callback):
        self.filename = filename
        assert(type(feature_names) == list)
        self.feature_names = [str(item) for item in feature_names]
        self.callback = callback

    def add_to_dict(self, the_dict):
        """Create a key, add self to dictionary"""
        key = "".join(self.feature_names)
        the_dict[key] = self

    def __str__(self):
        return dedent("""
        %s
        %s
        """) % (self.filename, self.feature_names)

    def compute(self, block):
        """Accept :ndarray: block, flatten and pass through to callback, which should return a vector of features."""
        return self.callback(block.ravel())


def interquatrile_range(data):
    Q1 = np.percentile(data, 25)
    Q3 = np.percentile(data, 75)
    return Q3 - Q1


class MapPlotter(object):
    def __init__(self, lonlat_map, geoms, block_info,
                 colour_mapper_callback, ax, src_image, filename=None):
        self.lonlat_map = lonlat_map
        self.block_info = block_info
        self.colour_mapper_callback = colour_mapper_callback
        self.geoms = geoms
        self.band = None
        self.ax = ax
        self.transform = None
        self.extent = None
        self._set_transform(src_image)
        self.ax.set_aspect('equal')
        self.filename = filename
        self._init_image()

    def render(self, rgb="#ff0000", alpha=1., mask=None):
        # get matrix
        # transform lon,lat to index. That's inverse transform.
        # set data at index
        # Alternatively.
        # Never store lat, lon, store matrix index instead.
        # where we need lat, lon, transform on the spot.
        # inverse transform lon, lat to px. Subtract 0.5 that was added during forward transform
        lonlat = self.lonlat_map if mask is None else self.lonlat_map[mask]
        mpx = ((np.array((lonlat[:, 0], lonlat[:, 1]) * ~self.T1) - 0) / 1).transpose()

        half_bs_x = self.block_info.step_x / 2
        half_bs_y = self.block_info.step_y / 2

        rgba = [int(c) for c in hex_to_uint8(rgb) + (alpha * 255,)]

        # TODO: vectorise?
        for (x, y) in np.round(mpx).astype(int):
            self.img[int(y-half_bs_y):int(y+half_bs_y+1), int(x-half_bs_x):int(x+half_bs_x+1)] = rgba

    def _init_image(self):
        """return empty image the size of master_src"""
        self.img = np.zeros((self.src_image.shape[0] // 1, self.src_image.shape[1] // 1, 4),
                            dtype=np.uint8)

    def plot(self, y_pred, alpha=1):
        """plot classification"""
        classes = np.unique(y_pred).astype(int)
        for class_ID in classes:
            mask = y_pred == class_ID
            rgb = map_class_ID_to_colour(class_ID)
            self.render(rgb, alpha, mask)
        self._finalise_plot()

    def plot_outlier(self, y_pred):
        mask = y_pred > 0  # +1 normal data -1 outlier
        print(mask.sum())
        self.render(rgb='#ffdb00', mask=mask, alpha=0.5)
        self._finalise_plot()

    def _set_transform(self, src_image):

        self.src_image = src_image
        #self.transform = src_image.affine
        #print("val", src_image.transform[:6])
        self.transform = src_image.transform[:6]
        #print("type", type(self.transform))
        #print("Val", *self.transform[:6])
        self.extent = rasterio.plot.plotting_extent(src_image)
        self.T1 = Affine.from_gdal(*self.transform) * Affine.translation(0.5, 0.5)

    def _finalise_plot(self):
        """Common stuff that needs to be done before saving the figure"""
        self.ax.set_xlim(self.extent[0:2])
        self.ax.set_ylim(self.extent[2:])
        self.ax.imshow(self.img, vmin=0, vmax=255, extent=self.extent)
        if self.filename is not None:
            plt.savefig(self.filename)

    #def load_background_tif(self, the_tif):
        #"""load background tif. We'll use its extent and transform."""
        #self.extent = rasterio.plot.plotting_extent(self.src)

    def save_geotiff(self, filename):
        print (rasterio.__version__)
        img = self.img.transpose((2, 0, 1))
        #np.savez(filename+".npz", img=img, transform=self.src_image.transform)
        with rasterio.open(
            filename,
            'w',
            driver='GTiff',
            compress="LZW",
            height=img.shape[1],
            width=img.shape[2],
            count=4,
            dtype=np.uint8,
            crs='+proj=latlong',
            transform=self.src_image.transform,
        ) as dst:
            dst.write(img.astype(np.uint8), indexes=(1, 2, 3, 4))

    def save_img(self, filename, background_filename):

        src, band, _ = load_src_and_band(background_filename, band_ID=1)

#        img = np.zeros(band.shape, dtype=np.uint8)
#        img = np.random.uniform(0, 255, size=band.size*4).reshape((*band.shape, 4)).astype(np.float64)

        band[band == src.nodata] = 12
        print("band_max", band.max())
        print("img max", self.img.max())
        import copy
        img = copy.copy(self.img) * 0.5
        for i in range(3):
            img[:,:,i] += band.astype(float) * (255 / 12. * 0.5)
        print("MAAAX", img.max())
        img[:,:,3] = 255.
        plt.imsave(filename, img.astype(np.uint8))

        #self.ax.imshow(band, cmap=plt.cm.gray, alpha=alpha,
        #               extent=rasterio.plot.plotting_extent(src))

    def plot_scale(self, x0, y0, dy, km, color='b'):
        c = 2 * np.pi * earth_radius_m * np.cos(np.deg2rad(y0))
        y1 = y0 + dy
        x1 = x0 + 360. / c * km * 1000
        self.ax.plot((x0, x0, x1, x1), (y1, y0, y0, y1), '-', lw=1, color=color)
        self.ax.text((x1+x0)/2, y1, r"$%i$ $\mathrm{km}$" % km,
                     horizontalalignment='center', color=color)

    def plot_block(self, size, offset_px, transform=None, circular_mask=False):
        """Plot rectangle indicating block size"""
        if len(offset_px) != 2:
            raise ValueError("Offset must be len=2")
        if transform is None:
            if self.transform is None:
                raise ValueError("Can't plot block. Either provide transform, "
                                 "or call load_background_tif() beforehand")
        # see https://pypi.org/project/affine/
        T1 = Affine.from_gdal(*self.transform) * Affine.translation(0.5, 0.5)

        # unit polygon, centred at 0,0
        if circular_mask:
            angle = np.linspace(0, np.pi*2, 40, endpoint=True)
            poly_unit = 0.5 * np.vstack((np.cos(angle), np.sin(angle))).transpose()
        else:
            poly_unit = np.array([[0, 0], [0, 1], [1, 1], [1, 0], [0, 0]]) - 0.5

        # scale and shift
        poly = poly_unit * size
        poly += offset_px

        # transform to Lon, Lat and plot
        X, Y = ((poly[:, 0], poly[:, 1]) * T1)
        self.ax.plot(X, Y, 'y-', lw=1.5)
        return poly


    def plot_background(self, image_filename, alpha=1.):
        src, band, _ = load_src_and_band(image_filename, band_ID=1)
        band[band == src.nodata] = 12
        self.ax.imshow(band, cmap=plt.cm.gray, alpha=alpha,
                       extent=rasterio.plot.plotting_extent(src))

    def plot_geoms(self):
        for i, the_class in enumerate(self.geoms):
            x, y = the_class.exterior.xy
            # internally, class labels start at 0
            self.ax.plot(x, y, color=self.colour_mapper_callback(i), lw=2., label=r"$%i$" % (i+1))
            self.ax.plot(x, y, 'w--', lw=1, dashes=(3, 4))

    def check_distance_metric(self, distance_metric_len):
        """Check if shape of distance metric matches number of classes

        If the ML algo fails to predicts all classes it has been trained on,
        distance_metric will lack elements. That would cause trouble downstream.
        Catch such differences here.

        Ideally we'd remove failed classes from geoms, and plot anyway.
        """
        if distance_metric_len < len(self.geoms):
            raise ValueError("distance_metric contains %i classes, but we have %i classes in geoms. "
                              "Most likely, one or more classes have not been found by the ML algorithm."
                             % (distance_metric_len, len(self.geoms)))

    def plot_distance(self, predicted, distance_metric, threshold, alpha=1):
        """plot classification, but only if their distance metric > threshold.

           The higher this threshold, the more certain the algo is
           Classifiers that support distance: SVC
        """
        #if len(self.geoms) == 2:
        #    distance_metric = np.vstack((distance_metric, distance_metric)).transpose()
        #    print("b"

        self.check_distance_metric(distance_metric.shape[1])

        mask_list = []

        for class_ID, _ in enumerate(self.geoms):
            this_class_distance = distance_metric[:, class_ID]
            mask = np.logical_and(this_class_distance > threshold,
                                  predicted == class_ID)
            mask_list.append(mask)

# FIXME: this should create an image, similar to plot_proba()
#            self.ax.scatter(self.lonlat_map[mask,0], self.lonlat_map[mask,1],
#                       c=map_class_ID_to_colour(class_ID),
#                       s=self.scatter_s, marker='s', edgecolors='face', alpha=alpha,
#                       linewidths=0)

        self.ax.legend()

        self._finalise_plot()

        return mask_list

    def plot_proba(self, proba, thresholds=(0.5, 0.9), alphas=(0.5, 1.)):
        """plot classification if probability is above given threshold.

           Classifiers that support probabilities: RF.

           Parameters
           ----------
           proba : array of shape [n_samples, n_classes]
                   Probability estimates as returned by clf.predict_proba()

           thresholds : list of thresholds
           alphas : list of corresponding alphas
        """

        self.check_distance_metric(proba.shape[1])

        for threshold, alpha in zip(thresholds, alphas):
            alpha *= 255
            for class_ID, _ in enumerate(self.geoms):
                pp = proba[:, class_ID]
                mask = pp > threshold
                rgb = [int(c) for c in hex_to_uint8(map_class_ID_to_colour(class_ID))]
                lonlat = self.lonlat_map[mask]
                mpx = ((np.array((lonlat[:, 0], lonlat[:, 1]) * ~self.T1) - 0) / 1).transpose()
                half_bs_x = self.block_info.step_x / 2
                half_bs_y = self.block_info.step_y / 2

                # TODO: vectorise?
                for (x, y) in np.round(mpx).astype(int):
                    self.img[int(y-half_bs_y):int(y+half_bs_y+1), int(x-half_bs_x):int(x+half_bs_x+1), 3] = alpha
                    self.img[int(y-half_bs_y):int(y+half_bs_y+1), int(x-half_bs_x):int(x+half_bs_x+1), :3] = rgb

        y = 1.025
        sy = 0.08
        sx = 0.06
        color = map_class_ID_to_colour(0)
        rect = patches.Rectangle((0, y), sx, sy, linewidth=1, edgecolor='k',
                                 facecolor='none', alpha=1, clip_on=False,
                                 transform=self.ax.transAxes)
        self.ax.text(0.07, y + 0.02, r'$< 40 \%$', transform=self.ax.transAxes)
        self.ax.add_patch(rect)

        rect = patches.Rectangle((0.22, y), sx, sy, facecolor=color, alpha=0.4,
                                 clip_on=False, transform=self.ax.transAxes)
        self.ax.text(0.29, y + 0.02, r'$40$-$80 \%$', transform=self.ax.transAxes)
        self.ax.add_patch(rect)

        rect = patches.Rectangle((0.44, y), sx, sy, facecolor=color, alpha=0.9,
                                 clip_on=False, transform=self.ax.transAxes)
        self.ax.text(0.51, y + 0.02, r'$> 80 \%$', transform=self.ax.transAxes)
        self.ax.add_patch(rect)

        self._finalise_plot()


if __name__ == "__main__":
    test_aggregate_histogram()
    test_aggregate_callback()
    test_map_class_ID_to_colour()
    print("OK")
