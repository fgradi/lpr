#!/usr/bin/env python3
# coding: utf-8

# - read tifs
# - aggregate into tiles
# - write csv or pickle

# ## installation
# ### dependencies
# try apt-get install python-rasterio python-geopandas. On Ubuntu 16.04 this brings in rasterio-0.31
#
# - install/upgrade via pip:
#     - pip install --upgrade enum34 requests rasterio geopandas
#
# If pip install rasterio fails:
# rasterio/_base.c:287:22: fatal error: cpl_conv.h: No such file or directory
# just apt-get install libgdal-dev
#
# apt install python3-sklearn
#

# x read geotif. We have many.
# x read shp. Only one, but with many classes in them.
# x rasterise shp, use as filter for tif
# remove 0 label
# - decompose into cells, aggregate: compute histogram
# write aggregated CSV or pickle

# 1 working code
# 2 bugfree code
# 3 documented code
# 4 optimised code

# we want: features vs samples
# - train_df: only shp regions
# - pred_df: everywhere

# list_of_tiles = create list of tiles. Should be standard operation for raster data.

# for the_tif in tifs:
#    read the_tif
#    for the_tile in list_of_tiles:
#        aggregate data on the_tile
#


# In[ ]:


#get_ipython().magic(u'load_ext autoreload')
#get_ipython().magic(u'autoreload 2')

import os
import sys
os.chdir("/home/albrecht/csiro/src/lpr")
sys.path.append(os.getcwd())
#sys.path.append("/mnt/hgfs/albrecht/lib/python/env/jupyter/lib/python2.7/site-packages") # FIXME: why is virtualenv path not picked up automatically?

import geopandas
import pandas as pd
import numpy as np
import matplotlib
#matplotlib.use("AGG") # prevent X11
import matplotlib.pyplot as plt
import scipy.stats.mstats
from pdb import pm
import numpy.testing as npt
from sklearn import svm
from sklearn import tree
from sklearn import ensemble
from sklearn.model_selection import cross_val_score, cross_validate
from sklearn.cluster import KMeans
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor
import string
from tictoc import Tic
import collections
import pub
from textwrap import dedent
from sklearn.metrics import confusion_matrix, precision_score

from common import FeatureCallback, FeatureHistogram, Aggregator, \
                   figsize, memory_usage_MB, \
                   get_equal_freq_bins, interquatrile_range, BlockInfo, \
                   MapPlotter, map_class_ID_to_colour, \
                   load_src_and_band, map_step_to_scatter_size

from importruntime import importRuntime
import session
import logging as log
import argparse
from hashlib import md5
from table import Table


# Plot style for Comp & Geosci
pub.SetPlotRC_CompGeosci()

parser = argparse.ArgumentParser(description="")
#parser.add_argument("-", action="store", type=float, default=0, help="")
parser.add_argument("-c", '--use-cache-only', action="store_true", help="Process only if cache exists")
parser.add_argument("-a", '--aggregate-only', action="store_true", help="Aggregate only, skip prediction.")
parser.add_argument("-v", "--verbose", dest="verbose_count", action="count",
                    default=0, help="increase verbosity")
parser.add_argument("-q", "--quiet", dest="quiet_count", action="count",
                    default=0, help="decrease verbosity")
parser.add_argument('session', metavar='file', type=str, nargs=1,
                    help='the session file')
args = parser.parse_args() #["-v", "session_c0_f42_t1"]) # remove for non-interactive use!
log.basicConfig(level=(max(3 - args.verbose_count + args.quiet_count, 0) * 10), # defaults to 30, i.e. WARN
                    format='%(levelname)s:%(message)s')

# In[ ]:
#bla
# In[ ]:
# read session file
args.session = args.session[0]
log.info("running on Python %s", sys.version_info)
log.info("Reading session %s", args.session)
session.read_from_file(args.session)
session.NAME = args.session
session.OUTDIR = os.path.join("png", session.NAME, "")
session.AGGREGRATE_ONLY = args.aggregate_only
session.USE_CACHE_ONLY = args.use_cache_only

log.info(session.summary())

pub.mkdirs(session.OUTDIR)

#bla

# In[ ]:
stats_fname = "stats_agg" if session.AGGREGRATE_ONLY else "stats"
stats_fname = session.OUTDIR + stats_fname + '.csv'

stats = Table(stats_fname)

# In[ ]:

# read land classes from shapefile
shapefile = geopandas.read_file(session.PATH_TO_LANDFORMS_SHP)
geoms_all = shapefile.geometry.values # list of shapely geometries

# re-order land classes
if session.FILTER_GEOMS is not None:
    geoms = [geoms_all[i] for i in session.FILTER_GEOMS]

# In[ ]:

relief_dict = {6: (46, 56, 66, 76),
               5: (35, 45, 55, 65, 75, 34, 44, 54, 64, 74),
               3: (33, 43, 53, 63),
               2: (22, 32, 42, 52),
               1: (11, 21, 31, 41),
               4: (51, 61, 71, 62, 72, 73)}

def relief_histogram(data):
    """map relief. Accept flattened data, return a vector"""

    # map data to relief_class
    keys = [key if item in value else 0 for item in data for key, value in relief_dict.items()]

    # compute histogram of relief class
    bins = [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5]
    return np.histogram(keys, bins=bins)[0]

def test_relief_histogram():
    npt.assert_array_equal(relief_histogram([11]), [1, 0, 0, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([22]), [0, 1, 0, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([33]), [0, 0, 1, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([51]), [0, 0, 0, 1, 0, 0])
    npt.assert_array_equal(relief_histogram([35]), [0, 0, 0, 0, 1, 0])
    npt.assert_array_equal(relief_histogram([46]), [0, 0, 0, 0, 0, 1])

test_relief_histogram()


# ## Assemble featureX dict

# In[ ]:


###############################################################################
## Assemble a dictionary of FeatureX classes, which describe potential features
## for our model, e.g. mrVBF divided into 10 bins.
## We assemble all possible features here, and later pick those which we
## actually use for training.
###############################################################################

fdict = {}

def add_to_dict(ftif):
    ftif.add_to_dict(fdict)

def path_to(the_tif):
    return os.path.join(session.PATH_TO_TIFS, the_tif)

if 1 and not session.QUICK:
    the_tif =  path_to("elev.tif")
    add_to_dict(FeatureCallback(the_tif, ["eskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["ekurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["eiqr"], callback=interquatrile_range))

bins = np.arange(11)
add_to_dict(FeatureHistogram( path_to("mrvbf.tif"), bins=bins, feature_prefix='vbf'))

add_to_dict(FeatureHistogram( path_to("mrrtf.tif"), bins=bins, feature_prefix='rtf'))


# equal freq binning
if 1 and not session.QUICK:
    n_bins = 5
    src, data, valid_data = load_src_and_band(the_tif)
    bins = get_equal_freq_bins(valid_data, n_bins)
    add_to_dict(FeatureHistogram(path_to("profcurv.tif"), bins=bins, feature_prefix='profcurv', feature_postfixes=np.arange(n_bins)+1))

##the_tif = "plancurv.tif"
##n_bins = 5
##src, data, valid_data = load_src_and_band(the_tif)
##bins = get_equal_freq_bins(valid_data, n_bins)
##add_to_dict(FeatureHistogram(the_tif, bins=bins, feature_prefix='plancurv', feature_postfix=np.arange(n_bins)+1)

 # topo wetness index
add_to_dict(FeatureCallback(path_to("twi.tif"), ["tmean"], callback=np.mean))

if 1 and not session.QUICK:
    the_tif = path_to("slope.tif")
    add_to_dict(FeatureCallback(the_tif, ["sskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["skurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["siqr"], callback=interquatrile_range))

if 1 and not session.QUICK:
    the_tif = path_to("wei.tif") # wind exposure index
    add_to_dict(FeatureCallback(the_tif, ["wskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["wkurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["wiqr"], callback=interquatrile_range))
    add_to_dict(FeatureCallback(the_tif, ["wmean"], callback=np.mean))

# slopeRCL
if 1 and not session.QUICK:
    # "slopeLE","slopeVG","slopeGE","slopeMO","slopeST","slopeVS",
    slope_bins  = [16, 26, 36, 46, 56, 66, 76]
    slope_names = ["LE", "VG", "GE", "MO", "ST", "VS", "CL"]
    bins=slope_bins
    #{"LE": 16, "VG": 26, "GE": 36, "MO": 46, "ST": 56, "VS": 66, "CL": 76}
    add_to_dict(FeatureHistogram(path_to("sloperelcl.tif"), bins=slope_bins, feature_prefix='slope', feature_postfixes=slope_names[:-1]))

#"reliefP","reliefR","reliefL","reliefH","reliefM",
# relief
if 0 and not session.QUICK:
    add_to_dict(FeatureCallback(path_to("sloperelcl.tif"), feature_names=['relP', 'relR', 'relL', 'relH', 'relM', 'relB'], callback=relief_histogram))


## Pick which features to train on. Either use all features...
ftifs = [val for val in fdict.values()]

## ... or cherry-pick
# TODO: monday.
if session.QUICK:
    ftifs = [ fdict['vbf'] ]
    #ftifs = [ fdict["sskewness"], fdict["skurtosis"] ]

#ftifs = [ fdict['wiqr'], fdict['siqr'], fdict['eiqr'] ]


# In[ ]:
def simple_mean_scorer(y_true, y_pred):
    """compute accuracy for each class, return average"""
    accuracy = []
    #log.info("%i unique classes", len(np.unique(y_true)))
    #log.info("len(y_pred) = %i len(y_true) = %i", len(y_true), len(y_pred))
    for class_ID in range(int(y_true.min()), int(y_true.max()+1)):
        mask = y_true == class_ID
        this_class_correct = (y_pred[mask] == y_true[mask]).sum()
        this_class_accuracy = this_class_correct / float(mask.sum())
        accuracy.append(this_class_accuracy)
        #log.info("c%i  %5i/%5i  %5.3f", class_ID, this_class_correct, mask.sum(), this_class_accuracy)
    accuracy = np.array(accuracy).mean()
    #print_confusion_matrix(y_true, y_pred, "CM")
    #log.info("acc %5.3f", accuracy)
    return accuracy

def print_confusion_matrix(y_true, y_pred, message=None):
    if message is not None:
        log.info(message)
    cm = confusion_matrix(y_true, y_pred)
    log.info('\n' + str(cm))
    cm_normed = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    np.set_printoptions(precision=3, suppress=True)
    log.info('\n' + str(cm_normed))
    return cm, cm_normed


#####################################################################################
## for interactive debugging in spyder, set interactive = 1 and uncomment 'if' below.
interactive = 0
# In[ ]:
if interactive:
    size = 51
    small_size = 25
    step = 50
    size = session.SIZES[0]
    step = session.STEPS[0]
# In[ ]:

#if interactive: # uncomment this for interactive use in spyder
def mk_map(size, step, small_size):
    ###############################################################################
    ## Aggregate training data into blocks
    ###############################################################################
    #global session

    block_info = BlockInfo(size_x=size, size_y=size, step_x=step, step_y=step,
                           small_size_x=small_size, small_size_y=small_size)
    #np.set_printoptions(precision=3, suppress=True)

    aggregator = Aggregator(block_info, ftifs, geoms,
                            circular_mask=session.CIRCULAR_MASK,
                            use_small_blocks=session.USE_SMALL_BLOCKS)

    stats["n_features"] = aggregator.n_features
    size_metres = np.array(aggregator.block_size_metres())

    log.info("size, step, small_size %i %i %i", size, step, small_size)
    log.info("average block size %5.2f km", size_metres.mean() / 1000.)

    stats["size"] = size
    stats["step"] = step
    stats["small_size"] = small_size
    stats["avg_block_km"] = (size_metres.mean() / 1000.)

    if not session.AGGREGRATE_ONLY:
        log.info("aggregate training ...")

        training_data = aggregator.aggregate(path_to_cache=os.path.join(session.PATH_TO_CACHE, "train"),
                                             csv_filename="quickt.csv")#, ax=None)
        log.info("%i features: %s ", aggregator.n_features, str(aggregator.hdr[:-3]))
        log.info("training matrix shape %s", str(training_data.shape))
        log.info("done.")


# In[ ]:
    ###############################################################################
    ## Aggregate entire map, by passing geoms=None
    ###############################################################################
    log.info("aggregate entire map...")
    timer = Tic()
    aggregator_map = Aggregator(block_info, ftifs, geoms=None,
                                circular_mask=session.CIRCULAR_MASK,
                                use_small_blocks=session.USE_SMALL_BLOCKS)
    entire_data = aggregator_map.aggregate(ax=None, csv_filename="quick.csv",
                                           path_to_cache=session.PATH_TO_CACHE)
    stats["t_aggregate_map"] = timer.toc()
    stats['mem_after_agg_MB'] = memory_usage_MB()

    log.info("feature matrix shape " + str(entire_data.shape))
    log.info("Done.")

# In[ ]:
    ## build vector of (training) class labels
    from shapely.geometry import Point
    def get_class_for_lonlat(feature_matrix, geoms):
        enumerated_geoms = list(enumerate(geoms))
        def _class(p):
            for i, the_geom in enumerated_geoms:
                if the_geom.contains(p):
                    return i
            return -1
        
        return [_class(Point(lonlat)) for lonlat in feature_matrix[:,-2:]]
    y_true = np.array(get_class_for_lonlat(entire_data, geoms))

# In[ ]:
    if session.AGGREGRATE_ONLY:
        stats.newline()
        return
# In[ ]:

    # a mask of nodata location. Used later to prevent plotting of classification
    # where source data is, e.g. ocean. Unfortunately, this approach won't work
    # because we filter feature matrix.
    # Options: (1) filter mask as well.
    #          (2) at plotting time, transform lonlat to px and test with mrVBF

    #mask = aggregator_map.invalid_mask()
# In[ ]:
    ###############################################################################
    ## Train. Follow sklearn terminology:
    ## X is matrix of training data, y is vector of labels
    ###############################################################################
    X_train = training_data[:,:-3]
    y_train = training_data[:,-3]
    X = entire_data[:,:-3]
    lonlat_map = entire_data[:,-2:]
    lonlat  = training_data[:,-2:]
    scatter_size = map_step_to_scatter_size[step]

    assert(np.isfinite(X_train).all())
    assert(np.isfinite(y_train).all())

    CLFs = collections.OrderedDict([
            ("OneclassSVM", svm.OneClassSVM(nu=0.01, kernel="rbf", gamma=1e-4)), # very good
            ("IsolationForest", IsolationForest(max_samples=1000, random_state=42, contamination=0.01)),
            ("LocalOutlierFactor", LocalOutlierFactor(n_neighbors=500))
    ])

    for clf_name, clf in CLFs.items():
            
        if clf_name == "LocalOutlierFactor":
            y_pred = clf.fit_predict(X)
        else:
            clf.fit(X_train)
            y_pred = clf.predict(X)
        
        #fig, (ax0, ax1) = plt.subplots(1,2, figsize=(30,6)); 
        fig, ax0 = plt.subplots(1,1,figsize=figsize)

        #plt.tight_layout()
        mp = MapPlotter(lonlat_map, scatter_size*1, geoms_all, block_info, map_class_ID_to_colour, ax0, 
                        aggregator.master_src, filename=None)
        #bins, hist, patches = ax1.hist(y_pred, bins=100)

        if session.PLOT_BACKGROUND:
            mp.plot_background(path_to("background_mrvbf.tif"), alpha=0.7)
        mp.plot_geoms()
        mp.plot_outlier(y_pred)

        if 1:
            map_filename = session.OUTDIR + "AFO_outlier_zoom_%s_size%03i_step%03i" % \
                                            (clf_name, size, step)
            plt.xlim(117.279, 120.5)
            plt.ylim(-35.146, -32.5)
            
            # override tic labels
            ax0.xaxis.set_ticklabels(['', '', r'$118^\circ \mathrm{E}$', '', r'$119^\circ\mathrm{E}$', '', r'$120^\circ\mathrm{E}$', ''])
            ax0.yaxis.set_ticklabels(['', r'$35^\circ \mathrm{S}$', '', r'$34^\circ\mathrm{S}$', '', r'$33^\circ\mathrm{S}$', ''])
        else:
            map_filename = session.OUTDIR + "AFO_outlier_%s_size%03i_step%03i" % \
                                            (clf_name, size, step)
        
        mp.plot_scale(119.8, -34.9, 0.05, 50)
        ax0.legend(ncol=9, loc="lower left",
                  markerscale=0.02, columnspacing=0.5,
                  handlelength=0.2, handletextpad=0.3, frameon=False,
                  bbox_to_anchor=(0.35, -0.029))
    

    
        plt.savefig(map_filename + '.png', dpi=300)
        plt.savefig(map_filename + '.pdf')

        plt.close()
        #mask_list = mp.plot_distance(y_pred, distance, threshold=1.5, filename=map_filename, alpha=0.5)
        stats.newline()

        if 0:
            ## plot final map. We use matplotlib's scatter plot.
            ## scatter_size is fine-tuned such that we still get a somewhat seamless map.
            #map_step_to_scatter_size = {50: 880, 25: 220, 12:43, 6:8.4, 3:2}
            #map_step_to_scatter_size = {100: 2.4, 50: 0.6, 25: 0.15} # production ready figures
            #map_step_to_scatter_size = {200:31.1, 100: 7.8, 50: 1.95, 25: 0.4861} # linewidths=0
            scatter_size = map_step_to_scatter_size[step]
    
            for i in range(len(geoms)):
                log.info("%02i  %5i  %5i", i, (y_train == i).sum(), (y_pred == i).sum())
                if (y_pred == i).sum() == 0:
                    log.warn(dedent("""FIXME: this is where we should remove--or better:
                        flag--classes that our algo fails to predict"""))
    
            #y_pred = np.random.randint(low=0, high=8, size=len(y_pred))
            c_predicted = map_class_ID_to_colour(y_pred)
    
            # record memory usage: after aggregation and training, but before plotting
            stats['mem_after_predict_MB'] = memory_usage_MB()
    
            fig, ax = plt.subplots(1,1,figsize=figsize)
            #scatter_size *= 20
            mp = MapPlotter(lonlat_map, scatter_size, geoms, map_class_ID_to_colour,
                            ax, aggregator.master_src, filename=None)
    
            if session.PLOT_BACKGROUND:
                mp.plot_background(path_to("background_mrvbf.tif"))
    ## In[ ]:
    
            # plot sample tile in lower right corner
            offset_px = (0.94*mp.src_image.width, 0.94*mp.src_image.height)
            mp.plot_block(size, offset_px, circular_mask=session.CIRCULAR_MASK)
            if session.USE_SMALL_BLOCKS:
                mp.plot_block(small_size, offset_px)
    
            # parameter string on top of plot
            ax.text(0., 1.05, param_str, transform=ax.transAxes)
    
            import matplotlib.ticker as ticker
            xloc = ticker.MultipleLocator(base=2.0) # this locator puts ticks at regular intervals
            yloc = ticker.MultipleLocator(base=2.0) # this locator puts ticks at regular intervals
            ax.xaxis.set_major_locator(xloc)
            ax.yaxis.set_major_locator(yloc)
    

# In[ ]:

#ex
#steps = [400, 200]; sizes = [11, 21]
#steps = [400]; sizes = [11]
#sizes = [25, 51]
for size in session.SIZES:
    for step in session.STEPS:
        t_per_op = 1.5e-8
        n_op = (10000./step)**2 * size**2
        t_predicted = t_per_op * n_op
        log.info("t_predicted %g", t_predicted)
        mk_map(size=size, step=step, small_size=min(25, size))
log.info("All done.")
