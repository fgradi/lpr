#!/usr/bin/env python2.7
# coding: utf-8

"""A minimum example for landscape classification"""

# FIXME:
# - step, size should be in metres!

# ## installation
# ### dependencies
# Tested with
# - geopandas 0.1.1
# - sklearn 0.19.1
# - rasterio 0.36.0
# 
# try apt-get install python-rasterio python-geopandas. 
# On Ubuntu 16.04 this brings in rasterio-0.31
# 
# - install/upgrade via pip:
#     - pip install --upgrade enum34 requests rasterio geopandas
#  
# If pip install rasterio fails:
# rasterio/_base.c:287:22: fatal error: cpl_conv.h: No such file or directory
# just apt-get install libgdal-dev
#
# latest sklearn:
#
# pip install --upgrade sklearn

import geopandas
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats.mstats
from pdb import pm
from sklearn import svm
from sklearn import tree
from sklearn.model_selection import cross_val_score
from sklearn.cluster import KMeans

from common import FeatureCallback, FeatureHistogram, Aggregator, \
                   path_to_landforms_shp, figsize, \
                   get_equal_freq_bins, interquatrile_range, BlockInfo, \
                   MapPlotter, map_class_ID_to_colour

import logging

logging.basicConfig(level=logging.WARN,
                    format='%(levelname)s:%(message)s')

# read land classes from shapefile
shapefile = geopandas.read_file(path_to_landforms_shp)
geoms = shapefile.geometry.values # list of shapely geometries
#geoms = geoms[4:] # use just two classes
geoms = [geoms[4], geoms[8]]
###############################################################################
## Assemble a dictionary of FeatureX classes, which describe potential features
## for our model, e.g. mrVBF divided into 10 bins.
## We assemble all possible features here, and later pick those which we 
## actually use for training.
###############################################################################
fdict = {}

def add_to_dict(ftif):
    ftif.add_to_dict(fdict)

the_tif = "elev.tif"
add_to_dict(FeatureCallback(the_tif, ["eskewness"], callback=scipy.stats.mstats.skew))
#add_to_dict(FeatureCallback(the_tif, ["ekurtosis"], callback=scipy.stats.mstats.kurtosis))
#add_to_dict(FeatureCallback(the_tif, ["eiqr"], callback=interquatrile_range))

the_tif = "twi.tif" # topo wetness index
add_to_dict(FeatureCallback(the_tif, ["tmean"], callback=np.mean))

bins = np.arange(11)
add_to_dict(FeatureHistogram("mrvbf.tif", bins=bins, feature_prefix='vbf'))
#add_to_dict(FeatureHistogram("mrrtf.tif", bins=bins, feature_prefix='rtf'))


## Pick which features to train on. Either use all features...
ftifs = [val for val in fdict.itervalues()] 

## ... or cherry-pick
#ftifs = [ fdict['vbf'] ]


###############################################################################
## Aggregate training data into blocks
###############################################################################
print "aggregate training ...", 

#step = 50; size = 51 # quick
#step = 12; size = 121
step = 12; size = 91
print "size, step", size, step

block_info = BlockInfo(size_x=size, size_y=size, step_x=step, step_y=step)
#np.set_printoptions(precision=3, suppress=True)
aggregator = Aggregator(block_info, ftifs, geoms)
training_data = aggregator.aggregate(csv_filename=None, ax=None)
print "done."

############################################################################### 
## Aggregate entire map, by passing geoms=None
###############################################################################
print "aggregate entire map..."
aggregator_map = Aggregator(block_info, ftifs, geoms=None)
entire_data = aggregator_map.aggregate(csv_filename=None, ax=None)
print "Done."

###############################################################################
## Train. Follow sklearn terminology: 
## X is matrix of training data, y is vector of labels
###############################################################################
train_array = training_data[:,:-3]
lonlat = training_data[:,-2:]

#clf = tree.DecisionTreeClassifier()
#clf_name = "DT"

clf = svm.SVC() #(kernel="poly", degree=3)
clf_name = "SVC"

print "training...", clf_name
X = training_data[:,:-3]
y = training_data[:,-3]
lonlat = training_data[:,-2:]

## k-fold cross-validation.
## k can't be greater than smallest sample size, so 
## count samples for each class
hist, _ = np.histogram(y, bins=np.arange(1,10))
k = min(hist.min(), 10)
if k >= 2:
    print "%i-fold CV" % k
    scores = cross_val_score(clf, X, y, cv=k)
    param_str = "size: %i  step: %i %s Acc: %0.2f (+/- %0.2f) %s"  \
        % (size, step, clf_name, scores.mean(), scores.std() * 2,
           str(scores))
else:
    param_str = "size: %i  step: %i %s no cross-valid possible"  \
        % (size, step, clf_name)
print param_str

clf.fit(X, y)
print "done. "

scatter_size = 5 # ... override anyway
mp = MapPlotter(lonlat, scatter_size, geoms, map_class_ID_to_colour, plt.gca())
mp.load_background_tif("mrvbf.tif")

c_y = map_class_ID_to_colour(y)
mp.plot(y, filename=None, alpha=0.5)
#bla

###############################################################################
## Predict entire map, then plot
###############################################################################
X = entire_data[:,:-3]
lonlat_map = entire_data[:,-2:]
predicted = clf.predict(X)
c_predicted = map_class_ID_to_colour(predicted)

## plot final map. We use matplotlib's scatter plot. 
## scatter_size is fine-tuned such that we still get a somewhat seamless map.
map_step_to_scatter_size = {50: 880, 25: 220, 12:43, 6:8.4}
scatter_size = map_step_to_scatter_size[step]*1.1
scatter_size = 15 # ... override anyway

fig, ax = plt.subplots(1,1,figsize=figsize)
mp = MapPlotter(lonlat_map, scatter_size, geoms, map_class_ID_to_colour, ax)
mp.load_background_tif("mrvbf.tif")
mp.plot_background()
mp.plot_geoms()
mp.plot_block(size)
ax.text(0.3, 0.005, param_str, transform=ax.transAxes)

map_filename = "AFO_pred%s_size%03i_step%02i.png" % (clf_name, size, step)
mp.plot(c_predicted, filename=map_filename, alpha=0.5)

for i in range(1, len(geoms)+1):
    print "%02i  %5i  %5i" % (i, (y == i).sum(), (predicted == i).sum())

print "All done."
