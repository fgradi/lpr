#!/usr/bin/env python3
# coding: utf-8

# - read tifs
# - aggregate into tiles
# - write csv or pickle

# ## installation
# ### dependencies
# try apt-get install python-rasterio python-geopandas. On Ubuntu 16.04 this brings in rasterio-0.31
#
# - install/upgrade via pip:
#     - pip install --upgrade enum34 requests rasterio geopandas
#
# If pip install rasterio fails:
# rasterio/_base.c:287:22: fatal error: cpl_conv.h: No such file or directory
# just apt-get install libgdal-dev
#
# apt install python3-sklearn
#

# x read geotif. We have many.
# x read shp. Only one, but with many classes in them.
# x rasterise shp, use as filter for tif
# remove 0 label
# - decompose into cells, aggregate: compute histogram
# write aggregated CSV or pickle

# 1 working code
# 2 bugfree code
# 3 documented code
# 4 optimised code

# we want: features vs samples
# - train_df: only shp regions
# - pred_df: everywhere

# list_of_tiles = create list of tiles. Should be standard operation for raster data.

# for the_tif in tifs:
#    read the_tif
#    for the_tile in list_of_tiles:
#        aggregate data on the_tile
#


# In[ ]:


#get_ipython().magic(u'load_ext autoreload')
#get_ipython().magic(u'autoreload 2')

import os
import sys
#sys.path.append(os.getcwd())
#sys.path.append("/mnt/hgfs/albrecht/lib/python/env/jupyter/lib/python2.7/site-packages") # FIXME: why is virtualenv path not picked up automatically?
sys.path.append("/home/tom/csiro/src/lpr/src")

import geopandas
import pandas as pd
import numpy as np
#matplotlib.use("AGG") # prevent X11
import matplotlib.pyplot as plt
import scipy.stats.mstats
from pdb import pm
import numpy.testing as npt
from sklearn import svm
from sklearn import tree
from sklearn import ensemble
from sklearn.model_selection import cross_val_score, cross_validate
from sklearn.cluster import KMeans
import string
from tictoc import Tic
import collections
import pub
from textwrap import dedent
from sklearn.metrics import confusion_matrix, precision_score

from common import FeatureCallback, FeatureHistogram, Aggregator, \
                   figsize, memory_usage_MB, \
                   get_equal_freq_bins, interquatrile_range, BlockInfo, \
                   MapPlotter, map_class_ID_to_colour, \
                   load_src_and_band

from importruntime import importRuntime
import session
import logging as log
import argparse
from hashlib import md5
from table import Table


# Plot style for Comp & Geosci
pub.SetPlotRC_CompGeosci()

parser = argparse.ArgumentParser(description="")
parser.add_argument("-c", '--use-cache-only', action="store_true", help="Process only if cache exists")
parser.add_argument("-a", '--aggregate-only', action="store_true", help="Aggregate only, skip prediction.")
parser.add_argument("-v", "--verbose", dest="verbose_count", action="count",
                    default=0, help="increase verbosity")
parser.add_argument("-q", "--quiet", dest="quiet_count", action="count",
                    default=0, help="decrease verbosity")
parser.add_argument('session', metavar='file', type=str, nargs=1,
                    help='the session file')
args = parser.parse_args() #["-v", "session_c0_f42_t1"]) # remove for non-interactive use!
log.basicConfig(level=(max(3 - args.verbose_count + args.quiet_count, 0) * 10), # defaults to 30, i.e. WARN
                    format='%(levelname)s:%(message)s')

# In[ ]:
# read session file
args.session = args.session[0]
log.info("running on Python %s", sys.version_info)
log.info("Reading session %s", args.session)
session.read_from_file(args.session)
session.NAME = os.path.basename(args.session)
session.OUTDIR = os.path.join("out", session.NAME, "")
session.AGGREGRATE_ONLY = args.aggregate_only
session.USE_CACHE_ONLY = args.use_cache_only

log.info(session.summary())
#print ("S", session.OUTDIR)
pub.mkdirs(session.OUTDIR)

# In[ ]:
stats_fname = "stats_agg" if session.AGGREGRATE_ONLY else "stats"
stats_fname = session.OUTDIR + stats_fname + '.csv'

stats = Table(stats_fname)

# In[ ]:

# read land classes from shapefile
shapefile = geopandas.read_file(session.PATH_TO_LANDFORMS_SHP)
geoms = shapefile.geometry.values  # list of shapely geometries
log.info(f"Read {len(geoms)} training regions from {session.PATH_TO_LANDFORMS_SHP}")

# re-order land classes
if session.FILTER_GEOMS is not None:
    geoms = [geoms[i] for i in session.FILTER_GEOMS]

# In[ ]:

relief_dict = {6: (46, 56, 66, 76),
               5: (35, 45, 55, 65, 75, 34, 44, 54, 64, 74),
               3: (33, 43, 53, 63),
               2: (22, 32, 42, 52),
               1: (11, 21, 31, 41),
               4: (51, 61, 71, 62, 72, 73)}


def relief_histogram(data):
    """map relief. Accept flattened data, return a vector"""

    # map data to relief_class
    keys = [key if item in value else 0 for item in data for key, value in relief_dict.items()]

    # compute histogram of relief class
    bins = [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5]
    return np.histogram(keys, bins=bins)[0]


def test_relief_histogram():
    npt.assert_array_equal(relief_histogram([11]), [1, 0, 0, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([22]), [0, 1, 0, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([33]), [0, 0, 1, 0, 0, 0])
    npt.assert_array_equal(relief_histogram([51]), [0, 0, 0, 1, 0, 0])
    npt.assert_array_equal(relief_histogram([35]), [0, 0, 0, 0, 1, 0])
    npt.assert_array_equal(relief_histogram([46]), [0, 0, 0, 0, 0, 1])


test_relief_histogram()


# ## Assemble featureX dict

# In[ ]:


###############################################################################
# Assemble a dictionary of FeatureX classes, which describe potential features
# for our model, e.g. mrVBF divided into 10 bins.
# We assemble all possible features here, and later pick those which we
# actually use for training.
###############################################################################

fdict = {}


def add_to_dict(ftif):
    ftif.add_to_dict(fdict)


def path_to(the_tif):
    return os.path.join(session.PATH_TO_TIFS, the_tif)


if 1 and not session.QUICK:
    the_tif =  path_to("elev.tif")
    add_to_dict(FeatureCallback(the_tif, ["eskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["ekurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["eiqr"], callback=interquatrile_range))

bins = np.arange(11)
add_to_dict(FeatureHistogram(path_to("mrvbf.tif"), bins=bins, feature_prefix='vbf'))

add_to_dict(FeatureHistogram(path_to("mrrtf.tif"), bins=bins, feature_prefix='rtf'))


# equal freq binning
if 1 and not session.QUICK:
    n_bins = 5
    src, data, valid_data = load_src_and_band(the_tif)
    bins = get_equal_freq_bins(valid_data, n_bins)
    add_to_dict(FeatureHistogram(path_to("profcurv.tif"), bins=bins, feature_prefix='profcurv',
                                 feature_postfixes=np.arange(n_bins)+1))

# the_tif = "plancurv.tif"
# n_bins = 5
# src, data, valid_data = load_src_and_band(the_tif)
# bins = get_equal_freq_bins(valid_data, n_bins)
# add_to_dict(FeatureHistogram(the_tif, bins=bins, feature_prefix='plancurv', feature_postfix=np.arange(n_bins)+1)

# topo wetness index
add_to_dict(FeatureCallback(path_to("twi.tif"), ["tmean"], callback=np.mean))

if 1 and not session.QUICK:
    the_tif = path_to("slope.tif")
    add_to_dict(FeatureCallback(the_tif, ["sskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["skurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["siqr"], callback=interquatrile_range))

if 1 and not session.QUICK:
    the_tif = path_to("wei.tif")  # wind exposure index
    add_to_dict(FeatureCallback(the_tif, ["wskewness"], callback=scipy.stats.mstats.skew))
    add_to_dict(FeatureCallback(the_tif, ["wkurtosis"], callback=scipy.stats.mstats.kurtosis))
    add_to_dict(FeatureCallback(the_tif, ["wiqr"], callback=interquatrile_range))
    add_to_dict(FeatureCallback(the_tif, ["wmean"], callback=np.mean))

# slopeRCL
if 1 and not session.QUICK:
    # "slopeLE","slopeVG","slopeGE","slopeMO","slopeST","slopeVS",
    slope_bins = [16, 26, 36, 46, 56, 66, 76]
    slope_names = ["LE", "VG", "GE", "MO", "ST", "VS", "CL"]
    bins = slope_bins
    add_to_dict(FeatureHistogram(path_to("sloperelcl.tif"), bins=slope_bins, feature_prefix='slope', feature_postfixes=slope_names[:-1]))

#"reliefP","reliefR","reliefL","reliefH","reliefM",
# relief
if 0 and not session.QUICK:
    add_to_dict(FeatureCallback(path_to("sloperelcl.tif"), feature_names=['relP', 'relR', 'relL', 'relH', 'relM', 'relB'], callback=relief_histogram))


## Pick which features to train on. Either use all features...
ftifs = [val for val in fdict.values()]

## ... or cherry-pick
if session.QUICK:
    ftifs = [ fdict['vbf'] ]
    #ftifs = [ fdict["sskewness"], fdict["skurtosis"] ]

#ftifs = [ fdict['wiqr'], fdict['siqr'], fdict['eiqr'] ]


# In[ ]:
def simple_mean_scorer(y_true, y_pred):
    """compute accuracy for each class, return average"""
    accuracy = []
    log.debug("%i unique classes", len(np.unique(y_true)))
    log.debug("len(y_pred) = %i len(y_true) = %i", len(y_true), len(y_pred))
    for class_ID in range(int(y_true.min()), int(y_true.max()+1)):
        mask = y_true == class_ID
        this_class_correct = (y_pred[mask] == y_true[mask]).sum()
        this_class_accuracy = this_class_correct / float(mask.sum())
        accuracy.append(this_class_accuracy)
        log.debug("c%i  %5i/%5i  %5.3f", class_ID, this_class_correct, mask.sum(), this_class_accuracy)
    accuracy = np.array(accuracy).mean()
    # print_confusion_matrix(y_true, y_pred, "CM")
    log.info("accuracy %5.3f", accuracy)
    return accuracy


def print_confusion_matrix(y_true, y_pred, message=None):
    if message is not None:
        log.info(message)
    cm = confusion_matrix(y_true, y_pred)
    log.info('\n' + str(cm))
    cm_normed = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    np.set_printoptions(precision=3, suppress=True)
    log.info('\n' + str(cm_normed))
    return cm, cm_normed


#####################################################################################
# for interactive debugging in spyder, set interactive = 1 and uncomment 'if' below.
interactive = 0
# In[ ]:
if interactive:
    size = 51
    small_size = 25
    step = 50
    size = session.SIZES[0]
    step = session.STEPS[0]
# In[ ]:


#if interactive: # uncomment this for interactive use in spyder
def mk_map(size, step, small_size):
    ###############################################################################
    # Aggregate training data into blocks
    ###############################################################################
    global session
    #np.set_printoptions(precision=3, suppress=True)

    block_info = BlockInfo(size, step, small_size)
    stats["size"] = size
    stats["step"] = step
    stats["small_size"] = small_size
    log.info("size, step, small_size %i %i %i", size, step, small_size)

    aggregator = Aggregator(block_info, ftifs, geoms,
                            circular_mask=session.CIRCULAR_MASK,
                            use_small_blocks=session.USE_SMALL_BLOCKS)

    stats["n_features"] = aggregator.n_features
    size_metres = np.array(aggregator.block_size_metres())

    log.info("average block size %5.2f km", size_metres.mean() / 1000.)
    stats["avg_block_km"] = (size_metres.mean() / 1000.)

    if not session.AGGREGRATE_ONLY:
        log.info("aggregate training ...")
        training_data = aggregator.aggregate(path_to_cache=os.path.join(session.PATH_TO_CACHE, "train"),
                                             csv_filename="quickt.csv")#, ax=None)

    y = training_data[:,-3]
    #print ("HIST", np.histogram(y, bins=np.array((-1,0,1,2,3,4,5,6,7,8,9,10)) - 0.5) )

# In[ ]:
    ###############################################################################
    # Aggregate entire map, by passing geoms=None
    ###############################################################################
    log.info("aggregate entire map...")
    timer = Tic()
    aggregator_map = Aggregator(block_info, ftifs, geoms=None,
                                circular_mask=session.CIRCULAR_MASK,
                                use_small_blocks=session.USE_SMALL_BLOCKS)
    entire_data = aggregator_map.aggregate(ax=None, csv_filename="quick.csv",
                                           path_to_cache=session.PATH_TO_CACHE)
    stats["t_aggregate_map"] = timer.toc()
    stats['mem_after_agg_MB'] = memory_usage_MB()

# In[ ]:
    ## build vector of (training) class labels
    from shapely.geometry import Point
    def get_class_for_lonlat(feature_matrix, geoms):
        enumerated_geoms = list(enumerate(geoms))
        def _class(p):
            for i, the_geom in enumerated_geoms:
                if the_geom.contains(p):
                    return i
            return -1

        return [_class(Point(lonlat)) for lonlat in feature_matrix[:,-2:]]
    y_true = np.array(get_class_for_lonlat(entire_data, geoms))

# In[ ]:
    if session.AGGREGRATE_ONLY:
        stats.newline()
        return
# In[ ]:

    # a mask of nodata location. Used later to prevent plotting of classification
    # where source data is, e.g. ocean. Unfortunately, this approach won't work
    # because we filter feature matrix.
    # Options: (1) filter mask as well.
    #          (2) at plotting time, transform lonlat to px and test with mrVBF

    #mask = aggregator_map.invalid_mask()
# In[ ]:
    ###############################################################################
    # Hyperparam opti
    ###############################################################################
    if 0:
        log.info("Hyperparam opti")
        from sklearn.model_selection import train_test_split
        from sklearn.model_selection import GridSearchCV
        from sklearn.metrics import classification_report

        # Split the dataset in two equal parts
        X = training_data[:,:-3]
        y = training_data[:,-3]

        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.5, random_state=0)

        # Set the arameters by cross-validation
        tuned_parpameters = [#{'kernel': ['linear'],
                             #'gamma': [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7],
                             #'C': [1, 10, 100]#
                             #, 1000, 10000, 100000, 1e6, 1e7, 1e8]},
                            {'kernel': ['linear'], 'C': [10000]}
                            ]

        scores = ['precision', 'recall']

        for score in scores:
            print("# Tuning hyper-parameters for %s" % score)
            print()

            clf = GridSearchCV(svm.SVC(), tuned_parameters, cv=5,
                               scoring='%s_macro' % score)
            clf.fit(X_train, y_train)

            print("Best parameters set found on development set:")
            print()
            print(clf.best_params_)
            print()
            print("Grid scores on development set:")
            print()
            means = clf.cv_results_['mean_test_score']
            stds = clf.cv_results_['std_test_score']
            for mean, std, params in zip(means, stds, clf.cv_results_['params']):
                print("%0.3f (+/-%0.03f) for %r"
                      % (mean, std * 2, params))
            print()

            print("Detailed classification report:")
            print()
            print("The model is trained on the full development set.")
            print("The scores are computed on the full evaluation set.")
            print()
            y_true, y_pred = y_test, clf.predict(X_test)
            print(classification_report(y_true, y_pred))
            print()
        stats.newline()
        return
# In[ ]:
    ###############################################################################
    # Train. Follow sklearn terminology:
    # X is matrix of training data, y is vector of labels
    ###############################################################################
    X_train = training_data[:, :-3]
    y_train = training_data[:, -3]

    assert(np.isfinite(X_train).all())
    assert(np.isfinite(y_train).all())

    # dict which maps clf_name : (method, is_unsupervised)
    CLFs = collections.OrderedDict([
            ("RF100", (ensemble.RandomForestClassifier(n_estimators=100, random_state=42, class_weight="balanced"), False)),
            ("lSVc1e1", (svm.LinearSVC(class_weight="balanced", C=1e1), False)),
            ("lSVc1e2", (svm.LinearSVC(class_weight="balanced", C=1e2), False)),
            ("lSVc1e3", (svm.LinearSVC(class_weight="balanced", C=1e3), False)),
            ("lSVc1e4", (svm.LinearSVC(class_weight="balanced", C=1e4), False)),
            ("SV", (svm.SVC(class_weight="balanced", gamma=1e-4, C=1e6), False)),
#            ("DT", (tree.DecisionTreeClassifier(random_state=42), False))
          ])

    for clf_name, (clf, is_unsupervised) in CLFs.items():
        stats["clf_name"] = clf_name
        stats["is_unsupervised"] = is_unsupervised

# In[ ]:
        log.info("training %s ...", clf_name)

        ###############################################################################
        # k-fold cross-validation.
        # k can't be greater than smallest sample size, so count samples for each class
        ###############################################################################
        k = 5
        hist, _ = np.histogram(y_train, bins=np.arange(0, len(geoms)))

        k = min(hist.min(), k)
        assert(hist.sum() == len(y_train))

        if session.CROSS_VALIDATE and k >= 2:
            # either use sklearn's cross-validate...
            if 0:
                log.info("%i-fold CV", k)
                averaging = ['micro', 'macro', 'weighted']
                scoring = [item+"_"+the_avg for item in ['precision', 'recall', 'f1'] for the_avg in averaging]
                scoring += ['accuracy']
                scores = cross_validate(clf, X_train, y_train, scoring=scoring,
                                        cv=k, return_train_score=False)
                accuracy = scores['test_accuracy']

                np.set_printoptions(precision=3)
                param_str = "size: %i  step: %i %s %i feat Acc: %0.3f (+/- %0.2f)\n%s"  \
                    % (size, step, clf_name, aggregator_map.n_features,
                       accuracy.mean(), accuracy.std() * 2,
                       string.join(["%3i" % int(item) for item in accuracy*1000]))

                for the_avg in averaging:
                    for item in ['precision', 'recall', 'f1']:
                        stats["%s_%s_mean" % (item, the_avg)] = scores['test_%s_%s' % (item, the_avg)].mean()
                        stats["%s_%s_std" % (item, the_avg)] = scores['test_%s_%s' % (item, the_avg)].std()
                stats["accuracy_mean"] = accuracy.mean()
                stats["accuracy_std"] = accuracy.std()

            else:  # ... or use home-brew scorer
                log.info("Using home-brew scorer k=%i", k)
                from sklearn.metrics import make_scorer

                scorer = make_scorer(simple_mean_scorer, greater_is_better=True)
                scores = cross_validate(clf, X_train, y_train, scoring=scorer,
                                        cv=k)  #, return_train_score=False)
                accuracy = scores['test_score']
                stats["accuracy_mean"] = accuracy.mean()
                stats["accuracy_std"] = accuracy.std()

                param_str = "size: %i  step: %i %s %i feat Acc: %0.3f (+/- %0.2f)\n%s" \
                    % (size, step, clf_name, aggregator_map.n_features,
                       accuracy.mean(), accuracy.std() * 2,
                       " ".join(["%3i" % int(item) for item in accuracy*1000]))
        else:
            param_str = "size: %i  step: %i %s %i feat " % (size, step, clf_name, aggregator_map.n_features)
            param_str += "no cross-valid possible" if session.CROSS_VALIDATE else "cross-valid disabled"

            stats["k-fold CV"] = -1
            stats["accuracy_mean"] = -1
            stats["accuracy_std"] = -1
# In[ ]:
        log.info(param_str)

        timer = Tic()
        if is_unsupervised:
            clf.fit(X_train)
        else:
            clf.fit(X_train, y_train)
        stats["t_train"] = timer.toc()

        log.info("done.")

# In[ ]:
        # there is a very nice plotting example
        # http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
        ###############################################################################
        # confusion matrix using entire training set (conceptually flawed!)
        ###############################################################################
        y_pred = clf.predict(X_train)
        _, cm_normed = print_confusion_matrix(y_train, y_pred,
                                              message="Confusion matrix using entire training set -- use with caution, conceptually flawed!")

        # accuracy: average of diagonal of confusion matrix
        stats["accuracy_diag_mean"] = cm_normed.diagonal().mean()
        stats["accuracy_diag_std"] = cm_normed.diagonal().std()
        log.info("accuracy_diag_mean = %g +- %g", stats["accuracy_diag_mean"], stats["accuracy_diag_std"])
        ###############################################################################
        # plot training
        ###############################################################################
        if 0:
            lonlat  = training_data[:,-2:]

            #c_predicted = map_class_ID_to_colour(y_pred)
            fig, ax = plt.subplots(1, 1, figsize=figsize)
            map_filename = session.OUTDIR + "AFO_pred%s_size%03i_step%03i.train.png" % (clf_name, size, step)
            mp = MapPlotter(lonlat, geoms, block_info,
                            map_class_ID_to_colour, ax, aggregator.master_src,
                            filename=None)
            #mp.plot(y_pred, alpha=0.5*255)
            mp.plot_geoms()
            # parameter string on top of plot
            ax.text(0., 1.05, param_str, transform=ax.transAxes)

            plt.savefig(map_filename, dpi=200)
            plt.close()


        ###############################################################################
        # Predict entire map, then plot
        ###############################################################################
        X = entire_data[:,:-3]
        lonlat_map = entire_data[:,-2:]

        timer = Tic()
        y_pred = clf.predict(X)
        stats["t_predict_map"] = timer.toc()

# In[ ]:
        ## accuracy on entire map
        acc_total = np.zeros(len(geoms))
        acc_correct = np.zeros_like(acc_total)

        mask = y_true > -1
        log.info("Total training samples %i", len(y_true[mask]))
        CM, CM_normed = print_confusion_matrix(y_true[mask], y_pred[mask], message="CM entire map for %s at %i %i" % (clf_name, size, step))
        #CM = np.zeros((len(geoms), len(geoms))
        for item_true, item_pred in zip(y_true, y_pred):
            if item_true > -1:
                acc_total[item_true] += 1
                #CM[item_true, item_pred] += 1
                if item_true == item_pred:
                    acc_correct[item_true] += 1
        accuracy_entire_diag = acc_correct/acc_total
        log.info("correct/total %s", str(accuracy_entire_diag))
        stats["accuracy_entire_diag_mean"] = accuracy_entire_diag.mean()
        stats["accuracy_entire_diag_std"] = accuracy_entire_diag.std()
        stats["CM"] = CM.flatten()
        stats["CM_normed"] = CM_normed.flatten()


# In[ ]:

        log.info("hash of prediction " + md5(y_pred).hexdigest())
#        stats["t_predict_map"] = timer.toc()

        for i in range(len(geoms)):
            log.info("%02i  %5i  %5i", i, (y_train == i).sum(), (y_pred == i).sum())
            if (y_pred == i).sum() == 0:
                log.warn(dedent("""FIXME: this is where we should remove--or better:
                    flag--classes that our algo fails to predict"""))

        #y_pred = np.random.randint(low=0, high=8, size=len(y_pred))
        #c_predicted = map_class_ID_to_colour(y_pred)

        # record memory usage: after aggregation and training, but before plotting
        stats['mem_after_predict_MB'] = memory_usage_MB()

        fig, ax = plt.subplots(1, 1, figsize=figsize)
        map_basename = session.OUTDIR + session.NAME + "_pred%s_size%03i_step%03i" % (clf_name, size, step)
        mp = MapPlotter(lonlat_map, geoms, block_info,
                        map_class_ID_to_colour, ax, aggregator.master_src,
                        filename=None)

        if session.PLOT_BACKGROUND:
            mp.plot_background(path_to("background_mrvbf.tif"))

# In[ ]:

        # plot sample tile in lower right corner
        offset_px = (0.94*mp.src_image.width, 0.94*mp.src_image.height)
        mp.plot_block(size, offset_px, circular_mask=session.CIRCULAR_MASK)
        if session.USE_SMALL_BLOCKS:
            mp.plot_block(small_size, offset_px)

        # parameter string on top of plot
        #ax.text(0., 1.05, param_str, transform=ax.transAxes)

        if session.PLOT_DISTANCE:
            if clf_name.startswith("RF"):
                proba = clf.predict_proba(X)
                thresholds = (0.40, 0.80)
                alphas = (0.4, 0.90)
                #thresholds = (0.25, 0.5, 0.75, 0.9)
                #thresholds = np.linspace(0.1, 0.9, 9)
             #   mp.plot_proba(proba, thresholds=(0.5, 0.9), alpha=0.5)
              #  mp.plot_proba(proba, thresholds=(0.5, 0.9), alphas=(0.4, 0.8))
                #thresholds = np.linspace(0.1, 1, 10)
                mp.plot_proba(proba, thresholds=thresholds, alphas=alphas)

            elif 0 and (clf_name.startswith("SV") or clf_name.startswith("lSV")):
                # Returns distance to the decision boundary, for each class.
                # The decision boundary separates a class from the rest.
                distance = clf.decision_function(X)

                # If we have only two classes, decision_function returns a single vector,
                # in which negative values correspond to one side of the decision boundary,
                # and positive to the other. Re-cast into a matrix for compatibility.
                if len(geoms) == 2:
                    distance = np.vstack((-distance, distance)).transpose()
                mask_list = mp.plot_distance(y_pred, distance, threshold=2., alpha=0.75)
            else:
                mp.plot(y_pred, alpha=0.5)
        else:
            mp.plot(y_pred, alpha=0.5)
        mp.plot_geoms()
        #mp.plot_scale(120, -34.5, 0.1, km=200)
        mp.plot_scale(135.4, -30.15, 0.025, km=50, color='w')
        #mp.plot_scale(135.4, -29.8, 0.025, km=50, color='k')

        mp.save_geotiff(map_basename+".tiff")
        #mp.save_img(map_basename+".tiff", background_filename=path_to("mrvbf.tif"))

        ax.legend(ncol=9, loc="lower left",
             markerscale=0.02, columnspacing=0.5,
             handlelength=0.2, handletextpad=0.3, frameon=False,
             bbox_to_anchor=(0.68, 0.97))#, textcolor='w')
#        ax.legend(ncol=9, loc="lower left",
#             markerscale=0.02, columnspacing=0.5,
#             handlelength=0.2, handletextpad=0.3, frameon=True,
#             bbox_to_anchor=(0.23, -0.029))#, textcolor='w')

        import matplotlib.ticker as ticker
        xloc = ticker.MultipleLocator(base=0.5) # this locator puts ticks at regular intervals
        yloc = ticker.MultipleLocator(base=0.5) # this locator puts ticks at regular intervals
        ax.xaxis.set_major_locator(xloc)
        ax.yaxis.set_major_locator(yloc)

        # override tic labels
        #ax.xaxis.set_ticklabels(['', r'$118^\circ \mathrm{E}$', r'$120^\circ\mathrm{E}$', r'$122^\circ\mathrm{E}$', r'$124^\circ\mathrm{E}$'])
        #ax.yaxis.set_ticklabels(['', r'$34^\circ \mathrm{S}$', r'$32^\circ\mathrm{S}$', r'$30^\circ\mathrm{S}$', r'$28^\circ\mathrm{S}$'])

        plt.savefig(map_basename+".png", dpi=200)
        #plt.savefig(map_basename+".pdf", dpi=200)
        if not interactive:
            plt.close()
        #mask_list = mp.plot_distance(y_pred, distance, threshold=1.5, filename=map_filename, alpha=0.5)
        stats.newline()
# In[ ]:
    return


# In[ ]:

for size in session.SIZES:
    for step in session.STEPS:
        t_per_op = 1.5e-8
        n_op = (10000./step)**2 * size**2
        t_predicted = t_per_op * n_op
        log.info("t_predicted %g", t_predicted)
        mk_map(size=size, step=step, small_size=min(25, size))
log.info("All done.")
