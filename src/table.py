# -*- coding: utf-8 -*-
#!/usr/bin/env python2.7
# In[ ]:
import pandas as pd
import collections

class Table(object):
    """Build a table, row by row, one cell at a time. Call newline(), proceed with next row.

    On init, try to load csv.
    Add items
        t = Table("users.csv")
        t["name"] = "Paul"
        t.newline()
        t["name"] = "Jim"
        t.newline()

    Every call to newline() saves to csv.
    """
    def __init__(self, csv_fname):
        self.df = None
        self.csv_fname = csv_fname
        #self.newline()
        self.d = collections.OrderedDict()

        try:
            self.df = pd.read_csv(self.csv_fname)
        except IOError:
            self.df = None  # collect info about runs: classifier name, training time ...

    def __setitem__(self, key, value):
        print (">>> stats.setitem called")
        try:
            print (">>> df shape", self.df.shape)
        except:
            pass
        print (">>> d len", len(self.d))
        print (">>> %s = %s" % (key, value))
        self.d[key] = value

    def __getitem__(self, key):
        return self.d[key]

    def newline(self):
        # add stats to dataframe
        #print (">>> stats.newline called.")
        #try:
        #    print (">>> before", self.df.shape)
        #except:
        #    pass
        if self.df is None:
            self.df = pd.DataFrame([self.d.values()], columns=self.d.keys()) # note [] around values
        else:
            values_list = [item for item in self.d.values()]
            try:
                self.df.loc[len(self.df)] = values_list
                #self.d.values() # This used to work?! But now copies the entire self.d.values into EACH cell of self.df?
                
                #self.df.append(values_list)
            except ValueError as e:
                print (e)
                print ("Did you change stats? Remove existing stats file and run again.") 
        self.df.to_csv(self.csv_fname, index=False)

        #self.d = collections.OrderedDict()


# In[ ]:
def test_stats():
    stats = Table("s.csv")
    stats["five"] = 5
    stats["also"] = "bla"
    stats.newline()
    stats["five"] = 6
    stats["also"] = "nope"
    stats.newline()
    print ("OK")
if __name__ == "__main__":
    test_stats()

