import string
def importRuntime(modulename):
    """ Import a named object from a module in the context of this function.
    """
    # -- from "Importing from a Module Whose Name Is Determined at Runtime"
    #    Credit: Juergen Hermann
    if modulename[-3:] == '.py': modulename = modulename[:-3]

    try:
#    if 1:
        module = __import__(modulename, globals(), locals(  ), ['*'])
    except ImportError:
        print ("could not import", modulename)
        return None

    return module
    #print module.PX_PER_METER
    #global PX_PER_METER, PY_PER_METER
    #PX_PER_METER = vars(module)['PX_PER_METER']
    #PY_PER_METER = vars(module)['PY_PER_METER']
